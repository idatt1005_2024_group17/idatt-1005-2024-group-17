
module com.example.mattjeneste.test {
    requires org.junit.jupiter.api;
    requires junit;
    requires com.example.mattjeneste;
    requires java.sql;
    
    opens testClasses;
    exports testClasses;
}