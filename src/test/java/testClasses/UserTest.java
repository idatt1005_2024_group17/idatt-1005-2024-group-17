package testClasses;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.example.mattjeneste.module.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserTest {
  
  private User user;
  
  @BeforeEach
  public void setUp() {
    user = new User("test", "test@test.com", "password");
  }
  
  @Test
  public void testGetUsername() {
    assertEquals("test", user.getUsername());
  }
  
  @Test
  public void testGetEmail() {
    assertEquals("test@test.com", user.getEmail());
  }
  
  @Test
  public void testToString() {
    assertEquals("Username: test\nEmail: test@test.com", user.toString());
  }
}
