package testClasses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.UnitHandler.Units;
import org.junit.jupiter.api.Test;

public class IngredientTest {
  
  @Test
  public void testConstructor() {
    Ingredient ingredient = new Ingredient("Sugar", Units.GRAM, 1);
    assertEquals("Sugar", ingredient.getName());
    assertEquals(Units.GRAM, ingredient.getUnit());
  }
  
  @Test
  public void testErrors() {
    assertThrows(IllegalArgumentException.class, () -> new Ingredient("Sugar", Units.GRAM, -1));
    assertThrows(IllegalArgumentException.class, () -> new Ingredient("Sugar", null, 0));
    assertThrows(IllegalArgumentException.class, () -> new Ingredient(null, Units.GRAM, 0));
  }
}
