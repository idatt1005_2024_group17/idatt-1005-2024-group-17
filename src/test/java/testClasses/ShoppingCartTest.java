package testClasses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.ShoppingCart;
import com.example.mattjeneste.module.UnitHandler.Units;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for ShoppingCart.
 */
public class ShoppingCartTest {
  
  private ShoppingCart cart;
  private Ingredient ingredient1;
  private Ingredient ingredient2;
  
  /**
   * Setup method to initialize test objects before each test method.
   */
  @BeforeEach
  public void setUp() {
    cart = new ShoppingCart();
    ingredient1 = new Ingredient("Milk", Units.DECILITER, 1);
    ingredient2 = new Ingredient("Eggs", Units.PIECE, 2);
  }
  
  @Test
  public void testAddIngredient() {
    cart.addIngredient(ingredient1, 1);
    assertTrue(cart.contains(ingredient1));
  }
  
  @Test
  public void testRemoveIngredient() {
    cart.addIngredient(ingredient1, 1);
    cart.removeIngredient(ingredient1, 1);
    assertFalse(cart.contains(ingredient1));
  }
  
  @Test
  public void testClearCart() {
    cart.addIngredient(ingredient1, 1);
    cart.addIngredient(ingredient2, 2);
    cart.clear();
    assertFalse(cart.contains(ingredient1));
    assertFalse(cart.contains(ingredient2));
  }
  
  @Test
  public void testAddPlannedIngredient() {
    cart.addPlannedIngredient(ingredient2, 2);
    assertFalse(cart.contains(ingredient2));
    assertTrue(cart.getAllIngredientsInShoppingCart().contains(ingredient2));
  }
  
  @Test
  public void testClearPlannedIngredient() {
    cart.addPlannedIngredient(ingredient2, 2);
    cart.clearPlannedIngredients();
    assertFalse(cart.contains(ingredient2));
  }
  
  @Test
  public void testHashMapConstructor() {
    cart.addIngredient(ingredient1, 1);
    ShoppingCart cart2 = new ShoppingCart(cart.getIngredients());
    assertTrue(cart2.contains(ingredient1));
  }
  
  @Test
  public void testClearAll() {
    cart.addIngredient(ingredient1, 1);
    cart.addPlannedIngredient(ingredient2, 2);
    cart.clearAll();
    assertFalse(cart.getAllIngredientsInShoppingCart().contains(ingredient1));
    assertFalse(cart.getAllIngredientsInShoppingCart().contains(ingredient2));
  }
  
  @Test
  public void testToString() {
    cart.addIngredient(ingredient1, 1);
    cart.addPlannedIngredient(ingredient2, 2);
    assertEquals("Shopping Cart:\n" +
            "Milk: 1\n" +
            "\n" +
            "Planned Ingredients:\n" +
            "Eggs: 2\n"
        , cart.toString());
  }
}