package testClasses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.Inventory;
import com.example.mattjeneste.module.Recipe;
import com.example.mattjeneste.module.Session;
import com.example.mattjeneste.module.UnitHandler;
import com.example.mattjeneste.module.User;
import java.sql.SQLException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class tests the Session class.
 * To test this you need the local database to be running.
 */

public class SessionTest {
  
  private Session session;
  private final User user = User.testUser;
  private final Ingredient ingredient = new Ingredient("Milk", UnitHandler.Units.DECILITER, 1);
  private final Recipe recipe = new Recipe(1, "Pancakes", "Good ol' pancakes", "mix and fry them");
  
  @BeforeEach
  public void setUp() throws SQLException {
    session = new Session(user);
  }
  
  /**
   * Tests the session's ingredient and recipe databases. They should contain the ingredient and recipe.
   */
  
  @Test
  public void testSessionDatabase() {
    assertTrue(session.getIngredientDatabase().stream().anyMatch(
        i -> i.equals(ingredient)
    ));
    
    assertTrue(session.getRecipeDatabase().stream().anyMatch(
        r -> r.equals(recipe)
    ));
  }
  
  /**
   * Tests the setUser method. This also gets the user's data from the database.
   */
  
  @Test
  public void testSetUser() throws SQLException {
    assertEquals(user, session.getUser());
    
    // Tests for the ingredient in the inventory and shopping cart
    assertTrue(testInventory());
    assertTrue(testShoppingCart());
    
    // Tests for the recipe in the cookbook and planned recipes
    assertTrue(testCookbook());
    assertTrue(testPlannedRecipes());
  }
  
  @Test
  public void testConstructorWithoutUser() throws SQLException {
    session = new Session();
    assertThrows(IllegalStateException.class, () -> session.attemptGetUserData());
    session.setUser(user);
    assertTrue(user.equals(session.getUser()));
    
    // Tests for the ingredient in the inventory and shopping cart
    assertTrue(testInventory());
    assertTrue(testShoppingCart());
    
    // Tests for the recipe in the cookbook and planned recipes
    assertTrue(testCookbook());
    assertTrue(testPlannedRecipes());
  }
  
  @Test
  public void testPlanningFunctions() {
    session.unplanRecipe(recipe);
    assertFalse(session.isPlanned(recipe));
    
    session.planRecipe(recipe);
    assertTrue(session.isPlanned(recipe));
  }
  
  @Test
  public void testMoveShoppingCartToInventory() {
    session.moveShoppingCartToInventory();
    Inventory inventory = session.getInventory();
    assertTrue(inventory.getIngredients().keySet().stream().anyMatch(
        i -> inventory.getIngredients().get(i) == 2
    ));
    
    assertFalse(session.getShoppingCart().getIngredients().keySet().stream().anyMatch(
        i -> i.equals(ingredient)
    ));
  }
  
  @Test
  public void testCurrentRecipe() {
    assertNull(session.getCurrentRecipe());
    session.setCurrentRecipe(recipe);
    assertEquals(recipe, session.getCurrentRecipe());
  }
  
  @Test
  public void testCurrentSearchTerm() {
    assertNull(session.getCurrentSearchTerm());
    session.setCurrentSearchTerm("Pancakes");
    assertEquals("Pancakes", session.getCurrentSearchTerm());
  }
  
  public boolean testInventory() {
    return session.getInventory().getIngredients().keySet().stream().anyMatch(
        i -> i.equals(ingredient)
    );
  }
  
  public boolean testShoppingCart() {
    return session.getShoppingCart().getIngredients().keySet().stream().anyMatch(
        i -> i.equals(ingredient)
    );
  }
  
  public boolean testCookbook() {
    return session.getCookbook().getRecipes().stream().anyMatch(
        r -> r.equals(recipe)
    );
  }
  
  public boolean testPlannedRecipes() {
    return session.getPlannedRecipes().stream().anyMatch(
        r -> r.equals(recipe)
    );
  }
}
