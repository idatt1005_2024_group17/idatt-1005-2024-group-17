package testClasses; /**
 * A test class for the recipe class.
 *
 * @author Sara Taghypour
 * @version 1.0
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.mattjeneste.module.Recipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RecipeTest {
  
  private Recipe recipe;
  private Recipe recipe2;
  
  /**
   * Initialize a new Recipe object before each test.
   */
  @BeforeEach
  public void setUp() {
    recipe = new Recipe(1, "Pasta Carbonara", "A classic in Italy!",
        "1. Cook pasta\n2. Prepare sauce\n3. Combine pasta and sauce");
    recipe2 = new Recipe(1, "Pasta Carbonara", "A classic in Italy!",
        "1. Cook pasta\n2. Prepare sauce\n3. Combine pasta and sauce", "pasta.png");
  }
  
  /**
   * Test retrieving the title of the recipe.
   */
  @Test
  public void testGetTitle() {
    assertEquals("Pasta Carbonara", recipe.getTitle());
  }
  
  /**
   * Test retrieving the recipe description.
   */
  @Test
  public void testGetDescription() {
    assertEquals("A classic in Italy!", recipe.getDescription());
  }
  
  /**
   * Test retrieving instructions.
   */
  @Test
  public void testGetInstructions() {
    assertEquals("1. Cook pasta\n2. Prepare sauce\n3. Combine pasta and sauce",
        recipe.getInstructions());
  }
  
  /**
   * Test the equals method.
   */
  @Test
  public void testEquals() {
    Recipe recipe2 = new Recipe(1, "Pasta Carbonara", "A classic in Italy!",
        "1. Cook pasta\n2. Prepare sauce\n3. Combine pasta and sauce");
    assertTrue(recipe.equals(recipe2));
  }
  
  @Test
  public void testGetImagePath() {
    assertEquals("/images/default.jpg", recipe.getImagePath());
    assertEquals("/images/pasta.png", recipe2.getImagePath());
  }
  
}



