package testClasses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.mattjeneste.module.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Vector2DTest {
  
  private Vector2D vector;
  
  @BeforeEach
  public void setUp() {
    vector = new Vector2D(1, 2);
  }
  
  @Test
  public void testGetX() {
    assertEquals(1, vector.getX());
  }
  
  @Test
  public void testGetY() {
    assertEquals(2, vector.getY());
  }
  
  @Test
  public void testEquals() {
    Vector2D vector2 = new Vector2D(1, 2);
    assertTrue(vector.equals(vector2));
  }
  
  @Test
  public void testToString() {
    assertEquals("(1.0, 2.0)", vector.toString());
  }
}
