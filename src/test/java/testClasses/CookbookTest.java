package testClasses;

import com.example.mattjeneste.module.Cookbook;
import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.Recipe;
import com.example.mattjeneste.module.UnitHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CookbookTest {
  
  private Cookbook cookbook;
  private Recipe testRecipe;
  
  private Ingredient ingredient1;
  private Ingredient ingredient2;
  
  @BeforeEach
  public void setUp() {
    cookbook = new Cookbook();
    testRecipe = new Recipe(1, "Pasta Carbonara", "A classic in Italy!",
        "1. Cook pasta\n2. Prepare sauce\n3. Combine pasta and sauce");
    ingredient1 = new Ingredient("Pasta", UnitHandler.Units.GRAM, 100);
    ingredient2 = new Ingredient("Eggs", UnitHandler.Units.PIECE, 2);
    
    testRecipe.addIngredient(
        ingredient1,
        1
    );
    testRecipe.addIngredient(
        ingredient2,
        1
    );
    
    cookbook.addRecipe(testRecipe);
  }
  
  @Test
  public void testAddRecipe() {
    Recipe recipe = new Recipe(2, "Spaghetti Bolognese", "A hearty Italian pasta dish",
        "1. Cook pasta\n2. Prepare sauce\n3. Combine pasta and sauce");
    recipe.addIngredient(
        new Ingredient("Pasta", UnitHandler.Units.GRAM, 100),
        1
    );
    recipe.addIngredient(
        new Ingredient("Ground beef", UnitHandler.Units.GRAM, 200),
        1
    );
    cookbook.addRecipe(recipe);
    assert (cookbook.contains(recipe));
  }
  
  @Test
  public void testRemoveRecipe() {
    cookbook.removeRecipe(testRecipe);
    assert (!cookbook.contains(testRecipe));
  }
  
  @Test
  public void testGetRecipe() {
    assert (cookbook.getRecipes().get(0).equals(testRecipe));
  }
}
