package testClasses; /**
 * Test class for the inventory class.
 *
 * @author Sara Taghypour
 * @version 1.0
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.Inventory;
import com.example.mattjeneste.module.UnitHandler;
import java.sql.SQLException;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InventoryTest {
  
  private Inventory inventory;
  
  private Ingredient ingredient1;
  private Ingredient ingredient2;
  
  /**
   * Initialize a new Inventory object before each test.
   */
  @BeforeEach
  public void setUp() {
    inventory = new Inventory();
    ingredient1 = new Ingredient("Milk", UnitHandler.Units.DECILITER, 1);
    ingredient2 = new Ingredient("Egg", UnitHandler.Units.PIECE, 2);
  }
  
  /**
   * Test if inventory is initialized with an empty list of items.
   */
  @Test
  public void testDefaultConstructor() {
    assertTrue(inventory.getIngredients().isEmpty());
  }
  
  /**
   * Test whether inventory is initialized with test data, when useTest parameter is true.
   * (See inventory class).
   */
  @Test
  public void testConstructorWithTestParameter() throws SQLException {
    inventory.addIngredient(ingredient1, 1);
    inventory.addIngredient(ingredient2, 2);
    Set<Ingredient> ingredients = inventory.getIngredients().keySet();
    assertEquals(2, ingredients.size());
    for (Ingredient ingredient : ingredients) {
      if (ingredient.getName().equals("Milk")) {
        assertEquals(1, inventory.getIngredients().get(ingredient));
      } else if (ingredient.getName().equals("Egg")) {
        assertEquals(2, inventory.getIngredients().get(ingredient));
      }
    }
  }
}

