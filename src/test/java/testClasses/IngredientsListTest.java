package testClasses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.IngredientsList;
import com.example.mattjeneste.module.UnitHandler.Units;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IngredientsListTest {
  private IngredientsList ingredientsList;
  private Ingredient testIngredient;
  
  @BeforeEach
  public void setUp() {
    ingredientsList = new IngredientsList();
    testIngredient = new Ingredient("Sugar", Units.GRAM, 1);
  }
  
  @Test
  public void testAddIngredient() {
    ingredientsList.addIngredient(testIngredient, 1);
    assertTrue(ingredientsList.contains(testIngredient));
    assertEquals(1, ingredientsList.getIngredients().get(testIngredient));
  }
  
  @Test
  public void testRemoveIngredient() {
    ingredientsList.addIngredient(testIngredient, 1);
    ingredientsList.removeIngredient(testIngredient, 1);
    assertFalse(ingredientsList.contains(testIngredient));
  }
  
  @Test
  public void testRemoveIngredientWithZeroAmount() {
    ingredientsList.addIngredient(testIngredient, 1);
    ingredientsList.removeIngredient(testIngredient, 1);
    ingredientsList.removeIngredient(testIngredient, 1);
    assertFalse(ingredientsList.contains(testIngredient));
  }
  
  @Test
  public void testToString() {
    ingredientsList.addIngredient(testIngredient, 1);
    assertEquals("Sugar 1g\n", ingredientsList.toString());
  }
  
  @Test
  public void testAddHashMapIngredients() {
    IngredientsList ingredientsList2 = new IngredientsList();
    ingredientsList2.addIngredient(testIngredient, 1);
    ingredientsList.addIngredients(ingredientsList2.getIngredients());
    assertTrue(ingredientsList.contains(testIngredient));
    assertEquals(1, ingredientsList.getIngredients().get(testIngredient));
  }
}
