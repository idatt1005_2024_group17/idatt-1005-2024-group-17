module com.example.mattjeneste {
  requires javafx.controls;
  requires javafx.fxml;
  requires junit;
  requires org.junit.jupiter.api;
  requires java.sql;


  opens com.example.mattjeneste to javafx.fxml;
  exports com.example.mattjeneste.controller;
  opens com.example.mattjeneste.controller to javafx.fxml;
  exports com.example.mattjeneste.module;
  opens com.example.mattjeneste.module to javafx.fxml;
  exports com.example.mattjeneste;
}