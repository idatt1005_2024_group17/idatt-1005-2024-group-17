package com.example.mattjeneste.view;

import java.util.Objects;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class ResultNavigationFooter extends VBox {
  
  public Button previousButton;
  public Button nextButton;
  
  public ResultNavigationFooter(boolean previous, boolean next) {
    super();
    
    getStylesheets().add(
        Objects.requireNonNull(getClass().getResource(
            "resultNavigationFooterStyling.css"
        )).toExternalForm()
    );
    
    getStyleClass().add("resultNavigationFooter");
    
    HBox hBoxButtonContainer = new HBox();
    hBoxButtonContainer.getStyleClass().add("hBoxButtonContainer");
    
    previousButton = new Button("Previous");
    nextButton = new Button("Next");
    
    previousButton.getStyleClass().add("button");
    nextButton.getStyleClass().add("button");
    
    if (!previous) {
      previousButton.setDisable(true);
      previousButton.getStyleClass().add("disabled");
    }
    
    if (!next) {
      nextButton.setDisable(true);
      nextButton.getStyleClass().add("disabled");
    }
    
    hBoxButtonContainer.getChildren().add(previousButton);
    hBoxButtonContainer.getChildren().add(nextButton);
    
    VBox growBox = new VBox();
    growBox.setPrefHeight(10000);
    setVgrow(growBox, Priority.ALWAYS);
    getChildren().add(growBox);
    
    getChildren().add(hBoxButtonContainer);
  }
}
