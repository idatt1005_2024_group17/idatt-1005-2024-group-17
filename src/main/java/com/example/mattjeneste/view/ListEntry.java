package com.example.mattjeneste.view;

import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class ListEntry extends HBox {
  
  public Button subtractButton;
  public Button addButton;
  public Label amountLabel;
  
  /**
   * Creates a new ListEntry with the specified ingredient and ingredient list.
   *
   * @param labelName the name of the ingredient.
   */
  public ListEntry(String labelName, String unit) {
    super();
    setPadding(new Insets(0, 10, 0, 10));
    setAlignment(Pos.CENTER_LEFT);
    
    String css = this.getClass().getResource("styling.css").toExternalForm();
    getStylesheets().add(css);
    
    String styling =
        Objects.requireNonNull(getClass().getResource("vBoxListStyling.css")).toExternalForm();
    getStylesheets().add(styling);
    
    Label entryName = new Label(labelName);
    getChildren().add(entryName);
    
    Label unitLabel = new Label("(" + unit + ")");
    unitLabel.getStyleClass().add("unitLabel");
    getChildren().add(unitLabel);
    setHgrow(unitLabel, Priority.ALWAYS);
    
    HBox amountHBox = new HBox();
    amountHBox.setSpacing(7);
    amountHBox.setAlignment(Pos.CENTER_RIGHT);
    setHgrow(amountHBox, Priority.ALWAYS);
    
    amountLabel = new Label();
    amountLabel.setPrefWidth(20);
    amountLabel.setAlignment(Pos.CENTER);
    
    subtractButton = new Button("-");
    addButton = new Button("+");
    
    addButton.getStyleClass().add("button");
    subtractButton.getStyleClass().add("button");
    
    amountHBox.getChildren().addAll(subtractButton, amountLabel, addButton);
    
    getChildren().add(amountHBox);
  }
}
