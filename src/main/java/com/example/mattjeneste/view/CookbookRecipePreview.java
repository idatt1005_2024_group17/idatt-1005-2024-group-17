package com.example.mattjeneste.view;

import com.example.mattjeneste.module.Recipe;
import java.util.Objects;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CookbookRecipePreview extends VBox {
  
  public Button planButton;
  public Button removeButton;
  
  static double maxImageWidth = 150;
  static double maxImageHeight = 100;
  
  public CookbookRecipePreview(Recipe recipe, boolean isPlanned) {
    super();
    getStylesheets().add(Objects.requireNonNull(getClass().getResource("recipePreviewStyling.css"))
        .toExternalForm());
    getStyleClass().add("vBox");
    
    Image image = new Image(
        Objects.requireNonNull(getClass().getResource(recipe.getImagePath())).toExternalForm());
    ImageView imageView = new ImageView(image);
    imageView.setPreserveRatio(true);
    
    double imageWidth = image.getWidth();
    double imageHeight = image.getHeight();
    if (imageWidth / imageHeight > maxImageWidth / maxImageHeight) {
      imageView.setFitWidth(maxImageWidth);
    } else {
      imageView.setFitHeight(maxImageHeight);
    }
    
    HBox titleHBox = new HBox();
    titleHBox.getStyleClass().add("hBox");
    
    Label title = new Label(recipe.getTitle());
    title.getStyleClass().add("title");
    
    HBox buttonHBox = new HBox();
    buttonHBox.setAlignment(javafx.geometry.Pos.CENTER);
    buttonHBox.setSpacing(5);
    if (isPlanned) {
      planButton = new Button("Unplan");
    } else {
      planButton = new Button("Plan");
    }
    planButton.getStyleClass().add("button");
    
    removeButton = new Button("Remove");
    
    buttonHBox.getChildren().add(planButton);
    buttonHBox.getChildren().add(removeButton);
    
    getChildren().add(imageView);
    titleHBox.getChildren().add(title);
    getChildren().add(titleHBox);
    getChildren().add(buttonHBox);
  }
}
