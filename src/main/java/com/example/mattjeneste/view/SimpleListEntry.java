package com.example.mattjeneste.view;

import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.IngredientsList;
import com.example.mattjeneste.module.UnitHandler;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class SimpleListEntry extends HBox {
  
  public Button addButton;
  
  /**
   * Creates a new ListEntry with the specified ingredient.
   *
   * @param ingredient the ingredient to create the entry for.
   */
  public SimpleListEntry(Ingredient ingredient) {
    super();
    setPadding(new Insets(0, 10, 0, 10));
    setAlignment(Pos.CENTER_LEFT);
    
    String styling =
        Objects.requireNonNull(getClass().getResource("vBoxListStyling.css")).toExternalForm();
    getStylesheets().add(styling);
    
    Label entryName = new Label(ingredient.getName());
    getChildren().add(entryName);
    
    Label unitLabel = new Label("(" + UnitHandler.abbreviate(ingredient.getUnit()) + ")");
    unitLabel.getStyleClass().add("unitLabel");
    getChildren().add(unitLabel);
    setHgrow(entryName, Priority.ALWAYS);
    
    HBox amountHBox = new HBox();
    amountHBox.setSpacing(7);
    amountHBox.setAlignment(Pos.CENTER_RIGHT);
    setHgrow(amountHBox, Priority.ALWAYS);
    
    addButton = new Button("+");
    addButton.getStyleClass().add("button");
    addButton.setId("addButton");
    
    addButton.setStyle("-fx-background-color: #ffffff");
    addButton.setStyle("-fx-border-radius: 50");
    
    amountHBox.getChildren().add(addButton);
    
    getChildren().add(amountHBox);
  }
  
}

