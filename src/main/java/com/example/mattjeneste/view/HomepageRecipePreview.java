package com.example.mattjeneste.view;

import com.example.mattjeneste.module.Recipe;
import java.util.Objects;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class HomepageRecipePreview extends VBox {
  
  static double maxImageWidth = 150;
  static double maxImageHeight = 100;
  
  public HomepageRecipePreview(Recipe recipe) {
    super();
    getStylesheets().add(Objects.requireNonNull(getClass().getResource("recipePreviewStyling.css"))
        .toExternalForm());
    getStyleClass().add("vBox");
    
    Image image = new Image(
        Objects.requireNonNull(getClass().getResource(recipe.getImagePath())).toExternalForm());
    ImageView imageView = new ImageView(image);
    imageView.setPreserveRatio(true);
    
    double imageWidth = image.getWidth();
    double imageHeight = image.getHeight();
    if (imageWidth / imageHeight > maxImageWidth / maxImageHeight) {
      imageView.setFitWidth(maxImageWidth);
    } else {
      imageView.setFitHeight(maxImageHeight);
    }
    
    HBox titleHBox = new HBox();
    titleHBox.getStyleClass().add("hBox");
    
    Label title = new Label(recipe.getTitle());
    title.getStyleClass().add("title");
    
    Text description = new Text(recipe.getDescription());
    description.getStyleClass().add("description");
    
    getChildren().add(imageView);
    titleHBox.getChildren().add(title);
    getChildren().add(titleHBox);
    getChildren().add(description);
  }
  
}
