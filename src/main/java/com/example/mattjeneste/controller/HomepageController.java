package com.example.mattjeneste.controller;

import com.example.mattjeneste.module.Recipe;
import com.example.mattjeneste.view.HomepageRecipePreview;

/**
 * Controller for the homepage of the application.
 */

public class HomepageController extends GridRecipePageController {
  
  public void init() {
    recipes = session.getRecipeDatabase();
    preparePage();
  }
  
  public void addRecipePreviews() {
    int counter = 0;
    for (int i = index; i - index < gridPaneHeight * gridPaneWidth && i < recipes.size(); i++) {
      Recipe recipe = recipes.get(i);
      
      HomepageRecipePreview homepageRecipePreview = new HomepageRecipePreview(recipe);
      
      setRecipeRedirect(homepageRecipePreview, recipe);
      bodyGridPane.add(homepageRecipePreview, counter % gridPaneWidth, counter / gridPaneWidth);
      counter++;
    }
  }
}
