package com.example.mattjeneste.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.HBox;

/**
 * Controller for the inventory page of the application.
 */
public class InventoryController extends IngredientListWithSearchFieldController {
  
  @FXML
  HBox inventoryTitleHBox;
  
  public void refreshDisplayList() {
    displayIngredients = interactablePageIngredientsList.getIngredients();
  }
  
  public void init() {
    interactablePageIngredientsList = session.getInventory();
    displayIngredients = interactablePageIngredientsList.getIngredients();
    try {
      preparePage();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
