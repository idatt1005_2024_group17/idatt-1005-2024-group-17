package com.example.mattjeneste.controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 * Controller for the profile page of the application.
 */
public class YourProfileController extends ApplicationController {
  
  @FXML
  Text profileData;
  @FXML
  ImageView profileImage;
  
  /**
   * Initializes the YourProfileController and sets the profile data and image.
   */
  public void init() {
    profileData.setText(session.getUser().toString());
    
    //TODO: Add functionality to load profile image from database
    String path = getClass().getResource("/images/defaultProfilePic.jpg").toExternalForm();
    if (path != null) {
      profileImage.setImage(new Image(path));
    } else {
      System.out.println("Error loading image");
    }
  }
  
}
