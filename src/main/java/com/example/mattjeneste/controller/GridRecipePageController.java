package com.example.mattjeneste.controller;

import com.example.mattjeneste.module.Recipe;
import com.example.mattjeneste.module.Vector2D;
import com.example.mattjeneste.view.ResultNavigationFooter;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;

/**
 * Controller for pages that display recipes in a grid format.
 * Can be customized to display recipes in any way by extending this class and implementing the
 * addRecipePreviews method.
 */

abstract public class GridRecipePageController extends ApplicationController {
  
  @FXML
  VBox bodyVBox;
  
  /**
   * The GridPane that will contain the recipe previews.
   */
  GridPane bodyGridPane;
  
  /**
   * The number of rows in the GridPane.
   */
  int gridPaneHeight = 1;
  
  /**
   * The number of columns in the GridPane.
   */
  int gridPaneWidth = 3;
  
  /**
   * The index of the first recipe to display in the GridPane.
   */
  int index = 0;
  
  /**
   * The list of recipes to display.
   */
  ArrayList<Recipe> recipes;
  
  abstract void addRecipePreviews();
  
  /**
   * Prepares the page for display.
   * Throws a RuntimeException if the recipes attribute is not set.
   * Called from the overridden init method in the subclass.
   */
  public void preparePage() throws RuntimeException {
    if (recipes == null) {
      throw new RuntimeException("recipes is not set");
    }
    handleResize();
    showRecipes();
  }
  
  /**
   * Handles a window resize event.
   * Adjusts the number of rows and columns in the GridPane based on the new window size.
   * Calls the showRecipes method to update the display.
   */
  
  @Override
  public void handleResize() {
    Vector2D windowSize = session.getWindowSize();
    int newGridWidth = (int) (windowSize.getX() / 200);
    int newGridHeight = (int) ((windowSize.getY() - 250) / 150);
    
    if (
        newGridWidth != gridPaneWidth ||
            newGridHeight != gridPaneHeight
    ) {
      if (newGridWidth >= 3) {
        gridPaneWidth = newGridWidth;
      }
      if (newGridHeight >= 1) {
        gridPaneHeight = newGridHeight;
      }
      if (newGridWidth >= 3 || newGridHeight >= 1) {
        showRecipes();
      }
    }
  }
  
  /**
   * Show the recipes in the cookbook starting from the index attribute.
   */
  public void showRecipes() {
    bodyVBox.getChildren().clear();
    addGridPane();
    addRecipePreviews();
    addNavigationFooter();
  }
  
  /**
   * Adds a GridPane to the bodyVBox that will contain the recipe previews.
   * The GridPane will have gridPaneWidth columns and gridPaneHeight rows.
   */
  
  public void addGridPane() {
    bodyGridPane = new GridPane();
    bodyGridPane.setAlignment(Pos.CENTER);
    
    bodyGridPane.setHgap(10);
    bodyGridPane.setVgap(10);
    bodyGridPane.setPadding(new javafx.geometry.Insets(10));
    
    for (int i = 0; i < gridPaneWidth; i++) {
      ColumnConstraints columnConstraints = new ColumnConstraints();
      columnConstraints.setPercentWidth((double) 100 / gridPaneWidth);
      bodyGridPane.getColumnConstraints().add(columnConstraints);
    }
    
    for (int i = 0; i < gridPaneHeight; i++) {
      RowConstraints rowConstraints = new RowConstraints();
      bodyGridPane.getRowConstraints().add(rowConstraints);
    }
    
    bodyVBox.getChildren().add(bodyGridPane);
  }
  
  /**
   * Adds a footer to the bodyVBox that allows the user to navigate between pages of recipes.
   */
  
  public void addNavigationFooter() {
    boolean hasPrevious = index > 0;
    boolean hasNext = recipes.size() > gridPaneHeight * gridPaneWidth + index;
    
    ResultNavigationFooter footer = new ResultNavigationFooter(hasPrevious, hasNext);
    if (hasPrevious) {
      footer.previousButton.setOnAction(e -> {
        bodyVBox.getChildren().remove(footer);
        index = Integer.max(index - gridPaneHeight * gridPaneWidth, 0);
        showRecipes();
      });
    }
    if (hasNext) {
      footer.nextButton.setOnAction(e -> {
        bodyVBox.getChildren().remove(footer);
        index = Integer.min(index + gridPaneHeight * gridPaneWidth, recipes.size() - 1);
        showRecipes();
      });
    }
    bodyVBox.getChildren().add(footer);
  }
}
