package com.example.mattjeneste.controller;

import com.example.mattjeneste.module.Ingredient;
import com.example.mattjeneste.module.IngredientsList;
import com.example.mattjeneste.module.UnitHandler;
import com.example.mattjeneste.view.ListEntry;
import com.example.mattjeneste.view.ResultNavigationFooter;
import com.example.mattjeneste.view.SimpleListEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

/**
 * Controller for pages that display a list of ingredients with a search field.
 * Can be customized to display ingredients in any way by extending this class and implementing the
 * refreshDisplayList method.
 */

abstract public class IngredientListWithSearchFieldController extends ApplicationController {
  
  @FXML
  VBox searchResultVBox;
  @FXML
  VBox ingredientListVBox;
  @FXML
  TextField searchField;
  
  /**
   * The number of ingredients to display in the search results and inventory list.
   */
  protected int displayListLength = 9;
  private final AtomicInteger ingredientListIndex = new AtomicInteger(0);
  private final AtomicInteger searchIndex = new AtomicInteger(0);
  
  /**
   * The IngredientsList that the user can interact with.
   */
  protected IngredientsList interactablePageIngredientsList;
  /**
   * The ingredients to display in the inventory list.
   * This includes ingredients and amounts that cannot be removed
   */
  protected HashMap<Ingredient, Integer> displayIngredients;
  /**
   * The ingredients to display in the search results.
   */
  protected ArrayList<Ingredient> searchResults;
  
  abstract void refreshDisplayList();
  
  @Override
  public void handleResize() {
    double height = session.getWindowSize().getY() - 210;
    int newListLength = (int) height / 25;
    if (newListLength != displayListLength && newListLength >= 3) {
      displayListLength = newListLength;
      refreshIngredientList();
      if (searchResults != null) {
        refreshSearchResults();
      }
    }
  }
  
  /**
   * Prepares the page for display.
   * Throws an Exception if the pageIngredientsList attribute is not set.
   * Called from the overridden init method in the subclass.
   */
  
  public void preparePage() throws Exception {
    if (interactablePageIngredientsList == null) {
      throw new Exception("pageIngredientsList is not set");
    }
    
    handleResize();
    
    searchResultVBox.setSpacing(5);
    ingredientListVBox.setSpacing(5);
    refreshIngredientList();
    
    searchField.setOnKeyPressed(e -> {
      if (e.getCode() == KeyCode.ENTER) {
        search(searchField.getText());
      }
    });
  }
  
  /**
   * Searches for ingredients in the database that match the search string.
   *
   * @param searchString the string to search for.
   */
  public void search(String searchString) {
    searchIndex.set(0);
    searchResults = new ArrayList<>();
    session.getIngredientDatabase().forEach(ingredient -> {
      if (ingredient.getName().toLowerCase().contains(searchString.toLowerCase())) {
        searchResults.add(ingredient);
      }
    });
    refreshSearchResults();
  }
  
  /**
   * Refreshes the search results in the searchResultVBox.
   */
  
  public void refreshSearchResults() {
    searchResultVBox.getChildren().clear();
    searchIndex.set(Math.max(0, searchIndex.intValue()));
    for (int i = searchIndex.intValue();
         i < searchIndex.intValue() + displayListLength && i < searchResults.size(); i++) {
      SimpleListEntry entry =
          new SimpleListEntry(searchResults.get(i));
      
      final int j = i;
      entry.addButton.setOnAction(e -> {
        interactablePageIngredientsList.addIngredient(searchResults.get(j), 1);
        refreshIngredientList();
      });
      searchResultVBox.getChildren().add(entry);
    }
    
    searchResultVBox.getChildren().add(
        generateNavigationFooter(searchResults.size(), searchIndex)
    );
  }
  
  /**
   * Refreshes the inventory list, not the search results.
   */
  public void refreshIngredientList() {
    refreshDisplayList();
    ingredientListVBox.getChildren().clear();
    ArrayList<Ingredient> inventoryIngredients = new ArrayList<>(displayIngredients.keySet());
    ingredientListIndex.set(Math.max(0, ingredientListIndex.intValue()));
    
    for (int i = ingredientListIndex.intValue();
         i < ingredientListIndex.intValue() + displayListLength && i < inventoryIngredients.size();
         i++) {
      ListEntry entry = new ListEntry(inventoryIngredients.get(i).getName(),
          UnitHandler.abbreviate(inventoryIngredients.get(i).getUnit()));
      entry.amountLabel.setText(displayIngredients.get(inventoryIngredients.get(i)).toString());
      
      final int j = i;
      entry.subtractButton.setOnAction(e -> {
        interactablePageIngredientsList.removeIngredient(inventoryIngredients.get(j), 1);
        refreshIngredientList();
      });
      entry.addButton.setOnAction(e -> {
        interactablePageIngredientsList.addIngredient(inventoryIngredients.get(j), 1);
        refreshIngredientList();
      });
      
      ingredientListVBox.getChildren().add(entry);
    }
    
    ingredientListVBox.getChildren().add(
        generateNavigationFooter(inventoryIngredients.size(), ingredientListIndex)
    );
  }
  
  public ResultNavigationFooter generateNavigationFooter(int displayListSize,
                                                         AtomicInteger listIndex) {
    int atomicIntValue = listIndex.intValue();
    boolean hasPrevious = atomicIntValue > 0;
    boolean hasNext = displayListSize > displayListLength + atomicIntValue;
    
    ResultNavigationFooter resultNavigationFooter =
        new ResultNavigationFooter(hasPrevious, hasNext);
    if (hasPrevious) {
      resultNavigationFooter.previousButton.setOnAction(e -> {
        listIndex.getAndAdd(-displayListLength);
        refreshIngredientList();
        if (searchResults != null) {
          refreshSearchResults();
        }
      });
    }
    if (hasNext) {
      resultNavigationFooter.nextButton.setOnAction(e -> {
        listIndex.getAndAdd(displayListLength);
        refreshIngredientList();
        if (searchResults != null) {
          refreshSearchResults();
        }
      });
    }
    return resultNavigationFooter;
  }
}
