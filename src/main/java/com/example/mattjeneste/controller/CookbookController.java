package com.example.mattjeneste.controller;

import com.example.mattjeneste.module.Recipe;
import com.example.mattjeneste.view.CookbookRecipePreview;

/**
 * Controller for the Cookbook page.
 * Extends GridRecipePageController.
 */

public class CookbookController extends GridRecipePageController {
  
  public void init() {
    recipes = session.getCookbook().getRecipes();
    preparePage();
  }
  
  /**
   * Adds recipe previews to the bodyGridPane.
   */
  
  public void addRecipePreviews() {
    int counter = 0;
    for (int i = index; i - index < gridPaneHeight * gridPaneWidth && i < recipes.size(); i++) {
      Recipe recipe = recipes.get(i);
      
      boolean isPlanned = session.isPlanned(recipe);
      
      CookbookRecipePreview cookbookRecipePreview = new CookbookRecipePreview(recipe, isPlanned);
      
      if (isPlanned) {
        cookbookRecipePreview.planButton.setOnAction(e -> {
          session.unplanRecipe(recipe);
          showRecipes();
        });
      } else {
        cookbookRecipePreview.planButton.setOnAction(e -> {
          session.planRecipe(recipe);
          showRecipes();
        });
      }
      
      cookbookRecipePreview.removeButton.setOnAction(e -> {
        session.unplanRecipe(recipe);
        session.getCookbook().removeRecipe(recipe);
        showRecipes();
      });
      
      
      setRecipeRedirect(cookbookRecipePreview, recipe);
      bodyGridPane.add(cookbookRecipePreview, counter % gridPaneWidth, counter / gridPaneWidth);
      counter++;
    }
  }
}
