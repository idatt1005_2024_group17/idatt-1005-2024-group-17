package com.example.mattjeneste.controller;

import com.example.mattjeneste.module.Recipe;
import com.example.mattjeneste.module.Session;
import com.example.mattjeneste.module.Vector2D;
import java.io.IOException;
import java.util.HashMap;
import javafx.beans.value.ChangeListener;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Abstract class that all controllers extend from. Contains methods for switching pages and setting
 *
 * @author Gard Alhaug
 */

public abstract class ApplicationController {
  
  /**
   * Enum representing all the pages in the application
   */
  public enum Page {
    HOMEPAGE,
    INVENTORY,
    SHOPPING_CART,
    YOUR_PROFILE,
    RECIPE_VIEWER,
    LOGIN_PAGE,
    REGISTER_PAGE,
    COOKBOOK,
    RECIPE_SEARCH_RESULT
  }
  
  /**
   * A map of all the pages and their corresponding FXML file paths
   */
  public static HashMap<Page, String> pagePathMap = new HashMap<>() {{
    put(Page.HOMEPAGE, "view/homepage.fxml");
    put(Page.INVENTORY, "view/inventory.fxml");
    put(Page.SHOPPING_CART, "view/shoppingCart.fxml");
    put(Page.YOUR_PROFILE, "view/yourProfile.fxml");
    put(Page.RECIPE_VIEWER, "view/recipeViewer.fxml");
    put(Page.LOGIN_PAGE, "view/loginPage.fxml");
    put(Page.REGISTER_PAGE, "view/registerPage.fxml");
    put(Page.COOKBOOK, "view/cookbook.fxml");
    put(Page.RECIPE_SEARCH_RESULT, "view/recipeSearchResults.fxml");
  }};
  
  /**
   * A map of all the pages and their corresponding button ids in the top bar of every page
   */
  public static HashMap<Page, String> buttonPageMap = new HashMap<Page, String>() {{
    put(Page.HOMEPAGE, "#homepageButton");
    put(Page.INVENTORY, "#inventoryButton");
    put(Page.SHOPPING_CART, "#shoppingCartButton");
    put(Page.YOUR_PROFILE, "#yourProfileButton");
    put(Page.COOKBOOK, "#cookbookButton");
  }};
  
  protected Stage stage;
  protected Scene scene;
  protected Parent root;
  protected Page currentPage;
  protected FXMLLoader loader;
  
  protected Session session;
  
  @FXML
  TextField recipeSearchField;
  
  /**
   * Abstract method that all controllers must implement.
   * This method is supposed to contain the code that populates the said page with dynamic elements.
   */
  abstract public void init();
  
  public void handleResize() {
  }
  
  /**
   * Switches the page to the given page
   *
   * @param event   The event that triggered the page switch
   * @param session The current session
   * @param page    The Page to switch to
   * @throws IOException          if the page fxml file cannot be found
   * @throws NullPointerException if the event source is null
   */
  @FXML
  public void switchPage(Event event, Session session, Page page)
      throws IOException, NullPointerException {
    root = getRoot(pagePathMap.get(page));
    currentPage = page;
    setSession(session);
    setTopBarButtonActions();
    initPageController();
    setSceneAndStage(event);
  }
  
  /**
   * Switches the page to the login page.
   *
   * @param event The event that triggered the page switch
   * @throws IOException If the FXML file is not found
   */
  @FXML
  public void changeToLoginPage(Event event) throws IOException {
    Parent root = getRoot(pagePathMap.get(Page.LOGIN_PAGE));
    setRoot(root);
    setSceneAndStage(event);
    
    LoginPageController controller = loader.getController();
    controller.init();
  }
  
  /**
   * Switches the page to the register page.
   *
   * @param event The event that triggered the page switch
   * @throws IOException If the FXML file is not found
   */
  
  @FXML
  public void changeToRegisterPage(Event event) throws IOException {
    Parent root = getRoot(pagePathMap.get(Page.REGISTER_PAGE));
    setRoot(root);
    setSceneAndStage(event);
    RegisterPageController controller = loader.getController();
    controller.init();
  }
  
  /**
   * Initializes the page controller from the loader
   */
  
  public void initPageController() {
    ApplicationController controller = loader.getController();
    controller.setRoot(root);
    controller.setSession(session);
    controller.init();
    session.controller = controller;
  }
  
  /**
   * Sets the top bar button actions for all the pages
   */
  
  public void setTopBarButtonActions() {
    // Set the action for each button in the top bar
    buttonPageMap.keySet().forEach(page -> {
      try {
        Button button = (Button) root.lookup(buttonPageMap.get(page));
        if (page == currentPage) {
          button.getStyleClass().add("currentPageButton");
        }
        button.setOnAction(e -> {
          try {
            switchPage(e, session, page);
          } catch (IOException ioException) {
            ioException.printStackTrace();
          }
        });
      } catch (NullPointerException e) {
        System.out.println("Button not found for page: " + page);
      }
    });
    
    // Set the action for the recipe search field, redirecting to the search result page
    try {
      TextField recipeSearchField = (TextField) root.lookup("#recipeSearchField");
      recipeSearchField.setPromptText("Search in recipes");
      recipeSearchField.setOnKeyPressed(e -> {
        if (e.getCode().toString().equals("ENTER")) {
          try {
            session.setCurrentSearchTerm(recipeSearchField.getText());
            switchPage(e, session, Page.RECIPE_SEARCH_RESULT);
          } catch (IOException ioException) {
            ioException.printStackTrace();
          }
        }
      });
    } catch (NullPointerException e) {
      System.out.println("Recipe search field not found");
    }
  }
  
  /**
   * Sets the scene and stage for the given event
   *
   * @param event The event that triggered the page switch
   */
  
  protected void setSceneAndStage(Event event) {
    stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    // 15 and 37.6666666666 are the width and height of the window border, so to get the
    // correct window size we need to subtract these values from the stage width and height
    // We have not yet found a way to do this that is consistent between different operating systems
    // Currently configured for Windows 10
    scene = new Scene(root, stage.getWidth() - 15, stage.getHeight() - 37.6666666666);
    stage.setScene(scene);
    stage.show();
    
    if (session != null) {
      session.setWindowSize(new Vector2D(stage.getWidth(), stage.getHeight()));
      
      ChangeListener<Number> stageSizeListener = (observable, oldValue, newValue) -> {
        Vector2D newSize = new Vector2D(stage.getWidth(), stage.getHeight());
        if (!newSize.equals(session.getWindowSize())) {
          session.setWindowSize(newSize);
        }
      };
      
      stage.widthProperty().addListener(stageSizeListener);
      stage.heightProperty().addListener(stageSizeListener);
    }
  }
  
  /**
   * Sets the session for the controller
   */
  public void setSession(Session session) {
    this.session = session;
    if (session == null) {
      System.out.println("Session is null");
    }
  }
  
  /**
   * Sets the root for the controller
   */
  public void setRoot(Parent root) {
    this.root = root;
  }
  
  /**
   * Gets the root node for the given path
   *
   * @param path The path to the FXML file
   * @return The root of the FXML file
   * @throws IOException If the FXML file is not found
   */
  public Parent getRoot(String path) throws IOException {
    loader = new FXMLLoader(getClass().getResource(path));
    return loader.load();
  }
  
  /**
   * Sets the onMouseClicked event for a node to redirect to the recipe viewer page with the given recipe.
   *
   * @param node   The node to set the event on
   * @param recipe The recipe to redirect to
   */
  public void setRecipeRedirect(Node node, Recipe recipe) {
    node.setOnMouseClicked(e -> {
      try {
        session.setCurrentRecipe(recipe);
        switchPage(e, session, Page.RECIPE_VIEWER);
      } catch (NullPointerException | IOException e1) {
        System.out.println("Error switching to recipe viewer" + e1.getMessage());
      }
    });
  }
}
