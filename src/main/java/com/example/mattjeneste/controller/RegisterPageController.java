package com.example.mattjeneste.controller;

import com.example.mattjeneste.WelcomeScreenApplication;
import com.example.mattjeneste.module.Session;
import com.example.mattjeneste.module.User;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Controller for the register page of the application.
 */
public class RegisterPageController extends ApplicationController {
  
  @FXML
  Button registerButton;
  @FXML
  TextField registerUsernameTextField;
  @FXML
  PasswordField registerPasswordField;
  @FXML
  PasswordField registerConfirmPasswordField;
  @FXML
  Label registerErrorLabel;
  @FXML
  TextField registerEmailTextField;
  
  public RegisterPageController() {
    super();
    if (session == null) {
      session = new Session();
    }
  }
  
  /**
   * Initializes the RegisterPageController and adds an action for clicking the register button.
   */
  public void init() throws RuntimeException {
    registerButton.setOnAction(e -> {
      String username = registerUsernameTextField.getText();
      String password = registerPasswordField.getText();
      String confirmPassword = registerConfirmPasswordField.getText();
      String email = registerEmailTextField.getText();
      
      if (username.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
          email.isEmpty()) {
        registerErrorLabel.setText("Please fill in all fields");
        return;
      } else if (!email.matches("^[\\w-.]+@([\\w-]+\\.)+[\\w-]{2,4}$")) {
        registerErrorLabel.setText("Invalid email address");
        return;
      } else if (!password.equals(confirmPassword)) {
        registerErrorLabel.setText("Passwords do not match");
        return;
      } else {
        registerErrorLabel.setText("");
      }
      
      if (session.getDatabaseManager().emailExists(email)) {
        registerErrorLabel.setText("Email already registered with another user");
        return;
      }
      
      User user = new User(username, email, password);
      session.getDatabaseManager().registerNewUser(user, password);
      
      try {
        session.setUser(user);
      } catch (SQLException ex) {
        throw new RuntimeException(ex);
      }
      
      WelcomeScreenApplication.stage.setOnCloseRequest(t -> {
        session.getDatabaseManager().closeConnection();
        Platform.exit();
        System.exit(0);
      });
      
      try {
        switchPage(e, session, Page.HOMEPAGE);
      } catch (Exception ex) {
        throw new RuntimeException(ex);
      }
    });
  }
  
}
