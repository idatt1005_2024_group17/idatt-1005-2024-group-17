package com.example.mattjeneste.controller;

import com.example.mattjeneste.WelcomeScreenApplication;
import com.example.mattjeneste.module.Session;
import com.example.mattjeneste.module.User;
import java.io.IOException;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.WindowEvent;

/**
 * Controller for the login page of the application.
 */
public class LoginPageController extends ApplicationController {
  
  @FXML
  TextField loginEmailTextField;
  @FXML
  PasswordField loginPasswordField;
  @FXML
  Label registerRedirectLabel;
  @FXML
  Button loginButton;
  @FXML
  Label loginErrorLabel;
  
  public LoginPageController() {
    super();
    if (session == null) {
      session = new Session();
    }
  }
  
  /**
   * Initializes the LoginPageController and adds actions for pressing the enter key and clicking the login button.
   */
  public void init() {
    loginPasswordField.setOnKeyPressed(e -> {
      if (e.getCode().toString().equals("ENTER")) {
        attemptLoginAndChangePage(e);
      }
    });
    
    loginButton.setOnAction(this::attemptLoginAndChangePage);
    
    registerRedirectLabel.setOnMouseClicked(e -> {
      try {
        registerPageRedirect(e);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }
  
  /**
   * Attempts to log in with the email and password in the text fields. If successful, switches to the homepage.
   *
   * @param e the event that triggered the method
   */
  public void attemptLoginAndChangePage(Event e) {
    if (attemptLogin(loginEmailTextField.getText(), loginPasswordField.getText())) {
      try {
        WelcomeScreenApplication.stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
          @Override
          public void handle(WindowEvent t) {
            session.getDatabaseManager().closeConnection();
            Platform.exit();
            System.exit(0);
          }
        });
        String username = session.getDatabaseManager().getUsername(loginEmailTextField.getText());
        session.setUser(
            new User(username, loginEmailTextField.getText(), loginPasswordField.getText()));
        switchPage(e, session, Page.HOMEPAGE);
      } catch (IOException | SQLException ex) {
        throw new RuntimeException(ex);
      }
    } else {
      loginErrorLabel.setText("Login failed\nwrong email or password.");
    }
  }
  
  /**
   * Redirects to the register page.
   *
   * @param event the event that triggered the method
   * @throws IOException if the FXML file is not found
   */
  public void registerPageRedirect(MouseEvent event) throws IOException {
    root = getRoot(pagePathMap.get(Page.REGISTER_PAGE));
    setRoot(root);
    setSession(session);
    ((TextField) root.lookup("#registerEmailTextField")).setText(loginEmailTextField.getText());
    setSceneAndStage(event);
    ((RegisterPageController) loader.getController()).init();
  }
  
  /**
   * Attempts to log in with the given email and password.
   *
   * @param email    the email to log in with
   * @param password the password to log in with
   * @return true if the login was successful, false otherwise
   */
  public boolean attemptLogin(String email, String password) {
    setSession(session);
    return session.getDatabaseManager().attemptLogin(email, password);
  }
}
