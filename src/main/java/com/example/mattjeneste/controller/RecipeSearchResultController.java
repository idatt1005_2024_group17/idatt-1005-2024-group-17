package com.example.mattjeneste.controller;

import com.example.mattjeneste.module.Recipe;
import com.example.mattjeneste.view.HomepageRecipePreview;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Controller for the recipe search result page of the application.
 */
public class RecipeSearchResultController extends GridRecipePageController {
  
  public void init() {
    String searchTerm = session.getCurrentSearchTerm().toLowerCase();
    recipes = new ArrayList<>(session.getRecipeDatabase().stream()
        .filter(recipe -> recipe.getTitle().toLowerCase().contains(searchTerm))
        .collect(Collectors.toList()));
    preparePage();
  }
  
  public void addRecipePreviews() {
    int counter = 0;
    for (int i = index; i - index < gridPaneHeight * gridPaneWidth && i < recipes.size(); i++) {
      Recipe recipe = recipes.get(i);
      
      HomepageRecipePreview homepageRecipePreview = new HomepageRecipePreview(recipe);
      
      setRecipeRedirect(homepageRecipePreview, recipe);
      bodyGridPane.add(homepageRecipePreview, counter % gridPaneWidth, counter / gridPaneWidth);
      counter++;
    }
  }
}
