package com.example.mattjeneste.controller;

import com.example.mattjeneste.module.Recipe;
import java.io.IOException;
import java.util.Objects;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 * Controller for the recipe viewer page of the application.
 */
public class RecipeViewerController extends ApplicationController {
  
  @FXML
  Label recipeTitleLabel;
  @FXML
  Text recipeInstructionText;
  @FXML
  ImageView recipeImage;
  @FXML
  Button addButton;
  @FXML
  Button planButton;
  @FXML
  Text ingredientsText;
  
  /**
   * The recipe to display.
   */
  private Recipe recipe;
  
  /**
   * Initializes the RecipeViewerController and sets the recipe title, instructions, image, and ingredients.
   */
  
  public void init() {
    recipe = session.getCurrentRecipe();
    recipeTitleLabel.setText(recipe.getTitle());
    recipeInstructionText.setText(recipe.getInstructions());
    ingredientsText.setText(recipe.toString());
    
    String recipeImagePath =
        Objects.requireNonNull(getClass().getResource(recipe.getImagePath())).toExternalForm();
    Image recipeImage = new Image(recipeImagePath);
    this.recipeImage.setImage(recipeImage);
    
    refreshButtons();
  }
  
  /**
   * Refreshes the buttons on the page for adding/removing from the cookbook and planning/unplanning.
   */
  
  public void refreshButtons() {
    if (session.getCookbook().contains(recipe)) {
      addButton.setText("Remove from Cookbook");
      addButton.setOnAction(e -> {
        session.getCookbook().removeRecipe(recipe);
        session.unplanRecipe(recipe);
        refreshButtons();
      });
    } else {
      addButton.setText("Add to Cookbook");
      addButton.setOnAction(e -> {
        session.getCookbook().addRecipe(recipe);
        try {
          switchPage(e, session, Page.COOKBOOK);
        } catch (IOException ex) {
          throw new RuntimeException(ex);
        }
      });
    }
    
    if (session.isPlanned(recipe)) {
      planButton.setText("Unplan");
      planButton.setOnAction(e -> {
        session.unplanRecipe(recipe);
        refreshButtons();
      });
    } else {
      planButton.setText("Plan");
      planButton.setOnAction(e -> {
        session.getCookbook().addRecipe(recipe);
        session.planRecipe(recipe);
        refreshButtons();
      });
    }
  }
}

