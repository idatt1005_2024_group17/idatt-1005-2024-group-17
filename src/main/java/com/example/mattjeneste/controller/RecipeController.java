package com.example.mattjeneste.controller;


import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class RecipeController extends ApplicationController {
    @FXML
    VBox friedChickenVBox;
    @FXML
    VBox greekSaladVBox;
    @FXML
    private ImageView greekSaladImage;
    @FXML
    private ImageView friedChickenImage;

    @FXML
    public void init() {
        loadRecipeImages();
    }
    public void loadRecipeImages() {
        try {
            String greekSaladImagePath = getClass().getResource("/images/greeksalad.jpeg").toExternalForm();
            Image greekSaladImage = new Image(greekSaladImagePath);
            this.greekSaladImage.setImage(greekSaladImage);

            String friedChickenImagePath = getClass().getResource("/images/friedchicken.jpeg").toExternalForm();
            Image friedChickenImage = new Image(friedChickenImagePath);
            this.friedChickenImage.setImage(friedChickenImage);
        } catch (Exception e) {
            System.out.println("Error loading images: " + e);
        }
    }
}

