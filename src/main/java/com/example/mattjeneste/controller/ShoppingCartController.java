package com.example.mattjeneste.controller;

import java.io.IOException;
import java.util.Objects;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * Controller for the shopping cart page of the application.
 */
public class ShoppingCartController extends IngredientListWithSearchFieldController {
  
  @FXML
  HBox shoppingCartTitleHBox;
  
  public void refreshDisplayList() {
    displayIngredients =
        session.getShoppingCart().getAllIngredientsInShoppingCart().getIngredients();
  }
  
  public void init() {
    session.updatePlannedRecipes();
    interactablePageIngredientsList = session.getShoppingCart();
    displayIngredients =
        session.getShoppingCart().getAllIngredientsInShoppingCart().getIngredients();
    
    try {
      preparePage();
      
      Button transferButton = new Button("Move to inventory");
      transferButton.getStylesheets().add(
          Objects.requireNonNull(
              getClass().getResource(
                  "view/style/buttonStyling.css"
              )
          ).toExternalForm()
      );
      
      transferButton.getStyleClass().add("button");
      transferButton.getStyleClass().add("lightButton");
      
      transferButton.setOnAction(event -> {
        session.moveShoppingCartToInventory();
        try {
          switchPage(event, session, Page.INVENTORY);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        System.out.println("Moved shopping cart to inventory.");
      });
      
      HBox growBox = new HBox();
      HBox.setHgrow(growBox, javafx.scene.layout.Priority.ALWAYS);
      growBox.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
      growBox.setPadding(new javafx.geometry.Insets(5));
      growBox.getChildren().add(transferButton);
      
      shoppingCartTitleHBox.getChildren().add(growBox);
      
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
