package com.example.mattjeneste.controller;

import javafx.scene.Scene;

/**
 * Controller for the welcome screen of the application.
 */
public class WelcomeScreenController extends ApplicationController {
  
  public void init() {
  }
  
  public void setScene(Scene scene) {
    this.scene = scene;
  }
}
