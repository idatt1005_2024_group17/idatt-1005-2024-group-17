package com.example.mattjeneste;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WelcomeScreenApplication extends javafx.application.Application {
  
  public static Stage stage;
  
  @Override
  public void start(Stage stage) throws IOException {
    
    FXMLLoader fxmlLoader = new FXMLLoader(
        WelcomeScreenApplication.class.getResource("controller/view/welcomeScreen.fxml"));
    Scene scene = new Scene(fxmlLoader.load());
    
    WelcomeScreenApplication.stage = stage;
    
    stage.setTitle("Recipe Roulette");
    stage.setScene(scene);
    stage.show();
  }
  
  public static void main(String[] args) {
    launch();
  }
}