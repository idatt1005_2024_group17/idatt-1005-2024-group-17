package com.example.mattjeneste.module;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Represents a list of ingredients and their amounts.
 */
public class IngredientsList {
  private final HashMap<Ingredient, Integer> ingredients;
  
  /**
   * Creates an empty IngredientsList.
   */
  public IngredientsList() {
    this(new HashMap<>());
  }
  
  public IngredientsList(HashMap<Ingredient, Integer> ingredients) {
    this.ingredients = ingredients;
  }
  
  /**
   * Adds an ingredient to the list.
   *
   * @param ingredient the ingredient to add.
   * @param amount     the amount of the ingredient to add.
   */
  public void addIngredient(Ingredient ingredient, int amount) {
    if (ingredients.containsKey(ingredient)) {
      ingredients.put(ingredient, ingredients.get(ingredient) + amount);
    } else {
      ingredients.put(ingredient, amount);
    }
  }
  
  /**
   * Removes an ingredient from the list. Removes the entry entirely if the amount is 0 or less.
   *
   * @param ingredient the ingredient to remove a certain amount from.
   * @param amount     the amount of the ingredient to remove.
   */
  public void removeIngredient(Ingredient ingredient, int amount) {
    if (ingredients.containsKey(ingredient)) {
      if (ingredients.get(ingredient) - amount <= 0) {
        ingredients.remove(ingredient);
      } else {
        ingredients.put(ingredient, ingredients.get(ingredient) - amount);
      }
    }
  }
  
  public HashMap<Ingredient, Integer> getIngredients() {
    return ingredients;
  }
  
  public boolean contains(Ingredient ingredient) {
    return ingredients.keySet().stream().anyMatch(i -> i.equals(ingredient));
  }
  
  /**
   * Adds all the ingredients from a HashMap to this IngredientsList.
   *
   * @param ingredientsList the HashMap to add ingredients from.
   */
  public void addIngredients(HashMap<Ingredient, Integer> ingredientsList) {
    ingredientsList.forEach(this::addIngredient);
  }
  
  public void addIngredients(IngredientsList ingredientsList) {
    addIngredients(ingredientsList.getIngredients());
  }
  
  /**
   * Adds ingredients to the list from a ResultSet.
   * If the ingredient is not in existingIngredients, it is added to the list.
   * If the ingredient already exists, the reference to the existing ingredient is used.
   *
   * @param resultSet           The resultSet from the DatabaseManager to use
   * @param existingIngredients The list of existing ingredients to check for duplicate instances
   * @throws SQLException If an error occurs while accessing the resultSet
   */
  
  public void addIngredients(ResultSet resultSet, ArrayList<Ingredient> existingIngredients)
      throws SQLException {
    while (resultSet.next()) {
      if (existingIngredients.stream().noneMatch(ingredient -> {
        try {
          return ingredient.getId() == resultSet.getInt("ingredient.id");
        } catch (SQLException e) {
          throw new RuntimeException(e);
        }
      })) {
        existingIngredients.add(new Ingredient(
            resultSet.getString("name"),
            UnitHandler.Units.valueOf(resultSet.getString("unit")),
            resultSet.getInt("ingredient.id")
        ));
      }
      
      addIngredient(
          existingIngredients.stream().filter(ingredient -> {
            try {
              return ingredient.getId() == resultSet.getInt("ingredient.id");
            } catch (SQLException e) {
              throw new RuntimeException(e);
            }
          }).findFirst().get(),
          resultSet.getInt("quantity")
      );
    }
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    ingredients.forEach((ingredient, amount) ->
        sb
            .append(ingredient.getName())
            .append(" ")
            .append(amount)
            .append(UnitHandler.abbreviate(ingredient.getUnit()))
            .append("\n"));
    return sb.toString();
  }
  
  public void clear() {
    ingredients.clear();
  }
}