package com.example.mattjeneste.module;


import com.example.mattjeneste.controller.ApplicationController;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class represents a session of the application with all relevant information.
 */
public class Session {
  
  private User user;
  private Inventory inventory;
  private ShoppingCart shoppingCart;
  private Cookbook cookbook;
  private ArrayList<Recipe> plannedRecipes;
  private ArrayList<Ingredient> ingredientDatabase;
  private ArrayList<Recipe> recipeDatabase;
  private Recipe currentRecipe;
  private final DatabaseManager databaseManager;
  private Vector2D windowSize = new Vector2D(600, 400);
  
  /**
   * The controller for the currently active page.
   */
  public ApplicationController controller;
  private String currentSearchTerm;
  
  /**
   * Creates a session with the given user.
   *
   * @param user the user to create the session with.
   */
  public Session(User user) throws SQLException {
    this();
    setUser(user);
  }
  
  /**
   * Creates a session with no user.
   */
  public Session() {
    databaseManager = new DatabaseManager(this);
    initSession();
  }
  
  /**
   * Initializes the session with the ingredient and recipe databases.
   */
  public void initSession() {
    ingredientDatabase = databaseManager.getIngredients();
    recipeDatabase = databaseManager.getRecipes();
  }
  
  /**
   * Attempts to get the user's inventory and shopping cart from the database.
   */
  public void attemptGetUserData() throws SQLException {
    if (user == null) {
      throw new IllegalStateException("User must be set before attempting to get user data.");
    }
    
    inventory = databaseManager.getInventory();
    shoppingCart = databaseManager.getShoppingCart();
    cookbook = databaseManager.getCookbook();
    plannedRecipes = databaseManager.getPlannedRecipes();
  }
  
  public DatabaseManager getDatabaseManager() {
    return databaseManager;
  }
  
  public Inventory getInventory() {
    return inventory;
  }
  
  public ShoppingCart getShoppingCart() {
    return shoppingCart;
  }
  
  public Cookbook getCookbook() {
    return cookbook;
  }
  
  public ArrayList<Recipe> getPlannedRecipes() {
    return plannedRecipes;
  }
  
  public ArrayList<Ingredient> getIngredientDatabase() {
    return ingredientDatabase;
  }
  
  public ArrayList<Recipe> getRecipeDatabase() {
    return recipeDatabase;
  }
  
  public User getUser() {
    return user;
  }
  
  /**
   * Sets the user of the session and attempts to get the user's data.
   *
   * @param user the user to set.
   */
  public void setUser(User user) throws SQLException {
    this.user = user;
    attemptGetUserData();
  }
  
  /**
   * Checks if the given recipe is planned.
   *
   * @param recipe the recipe to check.
   * @return true if the recipe is planned, false otherwise.
   */
  
  public boolean isPlanned(Recipe recipe) {
    return plannedRecipes.stream().anyMatch(plannedRecipe -> plannedRecipe.equals(recipe));
  }
  
  /**
   * Un-plans the given recipe. If the recipe is not planned, do nothing.
   *
   * @param recipe the recipe to un-plan.
   */
  public void unplanRecipe(Recipe recipe) {
    for (Recipe r : plannedRecipes) {
      if (r.equals(recipe)) {
        plannedRecipes.remove(r);
        updatePlannedRecipes();
        System.out.println("Recipe " + recipe.getTitle() + " un-planned.");
        return;
      }
    }
  }
  
  /**
   * Plans the given recipe. If the recipe is already planned, do nothing.
   *
   * @param recipe the recipe to plan.
   */
  public void planRecipe(Recipe recipe) {
    //check for duplicates
    if (isPlanned(recipe)) {
      System.out.println("Recipe " + recipe.getTitle() + " is already planned.");
    } else {
      plannedRecipes.add(recipe);
      updatePlannedRecipes();
      System.out.println("Recipe " + recipe.getTitle() + " planned.");
    }
  }
  
  /**
   * Updates the planned ingredients in the shopping cart, adding the missing ingredients from the inventory.
   */
  
  public void updatePlannedRecipes() {
    HashMap<Ingredient, Integer> currentIngredients =
        (HashMap<Ingredient, Integer>) this.getInventory().getIngredients().clone();
    
    shoppingCart.clearPlannedIngredients();
    
    for (Recipe recipe : plannedRecipes) {
      HashMap<Ingredient, Integer> recipeIngredients = recipe.getIngredients();
      //Loops through the ingredients of the recipe, and adds them to the shopping cart if they are not in the inventory
      for (Ingredient ingredient : recipeIngredients.keySet()) {
        //if the ingredient is in the inventory, check if there is enough, if not add to shopping cart
        if (currentIngredients.containsKey(ingredient)) {
          if (currentIngredients.get(ingredient) - recipeIngredients.get(ingredient) < 0) {
            //add to shopping cart
            shoppingCart.addPlannedIngredient(ingredient,
                recipeIngredients.get(ingredient) - currentIngredients.get(ingredient));
            currentIngredients.remove(ingredient);
          } else {
            currentIngredients.put(ingredient,
                currentIngredients.get(ingredient) - recipeIngredients.get(ingredient));
          }
        }
        //if the ingredient is not in the inventory, add to shopping cart
        else {
          shoppingCart.addPlannedIngredient(ingredient, recipeIngredients.get(ingredient));
        }
      }
    }
  }
  
  /**
   * Moves the ingredients in the shopping cart to the inventory and clears the shopping cart.
   */
  
  public void moveShoppingCartToInventory() {
    inventory.addIngredients(shoppingCart.getAllIngredientsInShoppingCart());
    shoppingCart.clearAll();
  }
  
  public Recipe getCurrentRecipe() {
    return currentRecipe;
  }
  
  public void setCurrentRecipe(Recipe currentRecipe) {
    this.currentRecipe = currentRecipe;
  }
  
  /**
   * Sets the window size and calls the resize handler in the controller.
   *
   * @param windowSize the new window size.
   */
  public void setWindowSize(Vector2D windowSize) {
    this.windowSize = windowSize;
    controller.handleResize();
  }
  
  public Vector2D getWindowSize() {
    return windowSize;
  }
  
  public String getCurrentSearchTerm() {
    return currentSearchTerm;
  }
  
  public void setCurrentSearchTerm(String currentSearchTerm) {
    this.currentSearchTerm = currentSearchTerm;
  }
}
