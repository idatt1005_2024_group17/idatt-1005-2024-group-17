package com.example.mattjeneste.module;

import java.util.ArrayList;

/**
 * A class representing a cookbook containing recipes.
 *
 * @version 1.0
 */
public class Cookbook {
  
  /**
   * The list of recipes in the cookbook.
   */
  private final ArrayList<Recipe> recipes;
  
  /**
   * Construct a new Cookbook object with an empty list of recipes.
   */
  public Cookbook() {
    recipes = new ArrayList<>();
  }
  
  /**
   * Construct a new Cookbook object with the specified list of recipes.
   *
   * @param recipe the recipe to add to the cookbook
   */
  public void addRecipe(Recipe recipe) {
    //first check if the recipe already exists in the cookbook
    if (contains(recipe)) {
      System.out.println("Recipe " + recipe.getTitle() + " already exists in cookbook.");
    } else {
      recipes.add(recipe);
      System.out.println("Recipe " + recipe.getTitle() + " added to cookbook.");
    }
  }
  
  /**
   * Remove the specified recipe from the cookbook.
   * If the recipe is not in the cookbook, do nothing.
   *
   * @param recipe the recipe to remove from the cookbook
   */
  
  public void removeRecipe(Recipe recipe) {
    for (Recipe checkRecipe : recipes) {
      if (checkRecipe.equals(recipe)) {
        recipes.remove(checkRecipe);
        System.out.println("Recipe " + recipe.getTitle() + " removed from cookbook.");
        return;
      }
    }
  }
  
  public ArrayList<Recipe> getRecipes() {
    return recipes;
  }
  
  /**
   * Check if the cookbook contains the specified recipe using the Recipe.equals() method.
   *
   * @param recipe the recipe to check for
   * @return true if the cookbook contains the recipe, false otherwise
   */
  
  public boolean contains(Recipe recipe) {
    return recipes.stream().anyMatch(r -> r.equals(recipe));
  }
}
