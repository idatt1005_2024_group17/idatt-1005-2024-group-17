package com.example.mattjeneste.module;

public class User {
  
  private final String username;
  private final String email;
  private final String password;
  
  public static User testUser = new User("test", "test@test.com", "password");
  
  /**
   * Creates a user with the given username, email, and password.
   *
   * @param username the username of the user.
   * @param email    the email of the user.
   * @param password the password of the user.
   */
  public User(String username, String email, String password) {
    this.username = username;
    this.email = email;
    this.password = password;
  }
  
  public boolean equals(User user) {
    return this.username.equals(user.getUsername()) && this.email.equals(user.getEmail());
  }
  
  public String getUsername() {
    return username;
  }
  
  public String getEmail() {
    return email;
  }
  
  @Override
  public String toString() {
    return "Username: " + username + "\nEmail: " + email;
  }
}
