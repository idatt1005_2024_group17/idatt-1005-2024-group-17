package com.example.mattjeneste.module;

import java.util.HashMap;

/**
 * A class representing a shopping cart containing ingredients and their quantities.
 *
 * @author Sara Taghypour
 * @version 1.0
 */

public class ShoppingCart extends IngredientsList {
  
  private final HashMap<Ingredient, Integer> plannedIngredients;
  
  /**
   * Construct a new ShoppingCart object with an empty list of ingredients and quantities.
   */
  public ShoppingCart() {
    super();
    plannedIngredients = new HashMap<>();
  }
  
  /**
   * Construct a new ShoppingCart object with the specified list of ingredients and quantities.
   *
   * @param ingredients the HashMap of ingredients to add to the shopping cart
   */
  public ShoppingCart(HashMap<Ingredient, Integer> ingredients) {
    super(ingredients);
    plannedIngredients = new HashMap<>();
  }
  
  /**
   * Adds an ingredient to the planned ingredients list.
   *
   * @param ingredient the ingredient to add.
   * @param amount     the amount of the ingredient to add.
   */
  public void addPlannedIngredient(Ingredient ingredient, int amount) {
    if (contains(ingredient)) {
      plannedIngredients.put(ingredient, plannedIngredients.get(ingredient) + amount);
    } else {
      plannedIngredients.put(ingredient, amount);
    }
  }
  
  public void clearPlannedIngredients() {
    plannedIngredients.clear();
  }
  
  /**
   * Merges the two HashMaps plannedIngredients and ingredients and returns the result.
   *
   * @return the merged HashMap of ingredients and quantities
   */
  
  public IngredientsList getAllIngredientsInShoppingCart() {
    HashMap<Ingredient, Integer> allIngredients = new HashMap<>(plannedIngredients);
    getIngredients().forEach((ingredient, amount) -> {
      allIngredients.merge(ingredient, amount, Integer::sum);
    });
    return new IngredientsList(allIngredients);
  }
  
  /**
   * Clears the shopping cart and the planned ingredients list.
   */
  public void clearAll() {
    clear();
    clearPlannedIngredients();
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    
    sb.append("Shopping Cart:\n");
    getIngredients().forEach((ingredient, amount) -> {
      sb.append(ingredient.getName()).append(": ").append(amount).append("\n");
    });
    
    sb.append("\nPlanned Ingredients:\n");
    plannedIngredients.forEach((ingredient, amount) -> {
      sb.append(ingredient.getName()).append(": ").append(amount).append("\n");
    });
    
    return sb.toString();
  }
}

