package com.example.mattjeneste.module;

import com.example.mattjeneste.module.UnitHandler.Units;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * This class is responsible for managing the database connection and executing queries.
 * It also contains methods for getting the ingredients, recipes, inventory and shopping cart from the database.
 * It also contains methods for checking if the email exists and attempting to log in.
 *
 * @version 1.0
 * @since 1.0
 */

public class DatabaseManager {
  private final String username = "root";
  private final String password = "";
  
  private Connection connection;
  private final Session session;
  
  
  public DatabaseManager(Session session) {
    this.session = session;
    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      System.out.println("Driver not found: " + e.getMessage());
    }
    try {
      String url = "jdbc:mysql://localhost:3306/recipe_roulette?serverTimezone=UTC";
      connection = DriverManager.getConnection(url, username, password);
    } catch (Exception e) {
      System.out.println("An error occurred while connecting to the database: " + e.getMessage());
    }
  }
  
  /**
   * Executes a query on the database.
   *
   * @param query The query to execute.
   * @return The result set of the query.
   */
  
  public ResultSet executeQuery(String query) {
    try {
      Statement statement = connection.createStatement();
      return statement.executeQuery(query);
    } catch (Exception e) {
      System.out.println("An error occurred while executing the query: " + e.getMessage());
      return null;
    }
  }
  
  /**
   * Attempts to log in using the email and password.
   *
   * @param email    The email to log in with.
   * @param password The password to log in with.
   * @return True if the login was successful, false otherwise.
   */
  
  public boolean attemptLogin(String email, String password) {
    String query =
        "SELECT * FROM user WHERE email = '" + email + "' AND password = '" + password + "'";
    ResultSet result = executeQuery(query);
    try {
      return result.next();
    } catch (Exception e) {
      System.out.println("An error occurred while attempting to login: " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Checks the database to see if the email already exists.
   *
   * @param email The email to check.
   * @return True if the email exists, false otherwise.
   */
  
  public boolean emailExists(String email) {
    String query = "SELECT * FROM user WHERE email = '" + email + "'";
    ResultSet result = executeQuery(query);
    try {
      return result.next();
    } catch (Exception e) {
      System.out.println("An error occurred while checking if the email exists: " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Gets the ingredient database from the database.
   *
   * @return ArrayList<Ingredient>
   * The ingredient database.
   */
  
  public ArrayList<Ingredient> getIngredients() {
    String query = "SELECT * FROM ingredient";
    ResultSet result = executeQuery(query);
    ArrayList<Ingredient> ingredients = new ArrayList<>();
    try {
      while (result.next()) {
        ingredients.add(
            new Ingredient(result.getString("name"), Units.valueOf(result.getString("unit")),
                result.getInt("id")));
      }
    } catch (Exception e) {
      System.out.println("An error occurred while getting the ingredients: " + e.getMessage());
    }
    return ingredients;
  }
  
  /**
   * Gets all the recipes from the database.
   *
   * @return ArrayList<Recipe>
   * The recipe database.
   */
  
  public ArrayList<Recipe> getRecipes() {
    String query = "SELECT * FROM recipe";
    ResultSet result = executeQuery(query);
    ArrayList<Recipe> recipes = new ArrayList<>();
    try {
      while (result.next()) {
        int id = result.getInt("id");
        Recipe recipe = new Recipe(result);
        String ingredientQuery =
            "SELECT * FROM recipeIngredients INNER JOIN ingredient ON recipeIngredients.ingredientid = ingredient.id WHERE recipeid = " +
                id;
        ResultSet ingredientResult = executeQuery(ingredientQuery);
        recipe.addIngredients(ingredientResult, session.getIngredientDatabase());
        recipes.add(recipe);
      }
    } catch (Exception e) {
      System.out.println("An error occurred while getting the recipes: " + e.getMessage());
    }
    return recipes;
  }
  
  /**
   * Uses a User object to get the inventory from the database using the user email.
   *
   * @return Inventory
   * The inventory object.
   */
  
  public Inventory getInventory() throws SQLException {
    String query = """
        SELECT * FROM inventory INNER JOIN\s
            user ON inventory.userid = user.id\s
            INNER JOIN ingredient ON inventory.ingredientid = ingredient.id
            WHERE user.email = '""" + session.getUser().getEmail() + "'";
    
    ResultSet result = executeQuery(query);
    Inventory inventory = new Inventory();
    try {
      inventory.addIngredients(result, session.getIngredientDatabase());
    } catch (Exception e) {
      System.out.println("An error occurred while getting the inventory: " + e.getMessage());
    }
    return inventory;
  }
  
  /**
   * Uses a User object to get the shopping cart from the database using the user email.
   *
   * @return ShoppingCart
   * The shopping cart object.
   */
  
  public ShoppingCart getShoppingCart() {
    String query = """
        SELECT * FROM shoppingCart INNER JOIN\s
            user ON shoppingCart.userid = user.id\s
            INNER JOIN ingredient ON shoppingCart.ingredientid = ingredient.id
            WHERE user.email = '""" + session.getUser().getEmail() + "'";
    
    ResultSet result = executeQuery(query);
    ShoppingCart shoppingCart = new ShoppingCart();
    try {
      shoppingCart.addIngredients(result, session.getIngredientDatabase());
    } catch (Exception e) {
      System.out.println("An error occurred while getting the shopping cart: " + e.getMessage());
    }
    return shoppingCart;
  }
  
  /**
   * Gets the username for a user from the database using the email.
   *
   * @param email The email to get the username from.
   * @return String
   * The username.
   */
  
  public String getUsername(String email) {
    String query = "SELECT * FROM user WHERE email = '" + email + "'";
    ResultSet result = executeQuery(query);
    try {
      if (result.next()) {
        return result.getString("username");
      } else {
        return null;
      }
    } catch (Exception e) {
      System.out.println("An error occurred while getting the username: " + e.getMessage());
      return null;
    }
  }
  
  public int getUserId(String email) {
    String query = "SELECT * FROM user WHERE email = '" + email + "'";
    ResultSet result = executeQuery(query);
    try {
      if (result.next()) {
        return result.getInt("id");
      } else {
        return -1;
      }
    } catch (Exception e) {
      System.out.println("An error occurred while getting the user id: " + e.getMessage());
      return -1;
    }
  }
  
  /**
   * Registers a new user in the database.
   *
   * @param user     The user object to register.
   * @param password The password for the user.
   */
  
  public void registerNewUser(User user, String password) throws IllegalArgumentException {
    if (emailExists(user.getEmail())) {
      throw new IllegalArgumentException("Email already exists");
    }
    
    String query =
        "INSERT INTO user (username, email, password) VALUES ('" + user.getUsername() + "', '" +
            user.getEmail() + "', '" + password + "')";
    try {
      executeUpdate(query);
    } catch (Exception e) {
      System.out.println("An error occurred while registering the new user: " + e.getMessage());
    }
  }
  
  public Recipe getRecipe(int recipeId) {
    Recipe alreadyExistingRecipe = getAlreadyExistingRecipe(recipeId);
    if (alreadyExistingRecipe != null) {
      return alreadyExistingRecipe;
    }
    
    String query = "SELECT * FROM recipe WHERE id = " + recipeId;
    ResultSet result = executeQuery(query);
    try {
      if (result.next()) {
        Recipe recipe = new Recipe(result);
        String ingredientQuery =
            "SELECT * FROM recipeIngredients INNER JOIN ingredient ON recipeIngredients.ingredientid = ingredient.id WHERE recipeid = " +
                recipeId;
        ResultSet ingredientResult = executeQuery(ingredientQuery);
        recipe.addIngredients(ingredientResult, session.getIngredientDatabase());
        return recipe;
      } else {
        return null;
      }
    } catch (Exception e) {
      System.out.println("An error occurred while getting the recipe: " + e.getMessage());
      return null;
    }
  }
  
  public Recipe getAlreadyExistingRecipe(int recipeId) {
    return session.getRecipeDatabase()
        .stream()
        .filter(recipe -> recipe.getId() == recipeId)
        .findFirst()
        .orElse(null);
  }
  
  public Cookbook getCookbook() {
    int userId = getUserId(session.getUser().getEmail());
    String query = "SELECT * FROM cookbook WHERE userid = " + userId;
    
    ResultSet result = executeQuery(query);
    Cookbook cookbook = new Cookbook();
    try {
      //checks if the result is empty
      if (result.isBeforeFirst()) {
        while (result.next()) {
          // If recipe not in the sessions recipe list, then add it
          Recipe recipe = getRecipe(result.getInt("recipeid"));
          cookbook.addRecipe(recipe);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("An error occurred while getting the cookbook: " + e.getMessage());
    }
    return cookbook;
  }
  
  /**
   * Gets the planned recipes for the session user from the database.
   *
   * @return The planned recipes.
   */
  
  public ArrayList<Recipe> getPlannedRecipes() {
    int userId = getUserId(session.getUser().getEmail());
    String query = "SELECT * FROM plannedRecipes WHERE userid = " + userId;
    
    ResultSet result = executeQuery(query);
    ArrayList<Recipe> plannedRecipes = new ArrayList<>();
    try {
      //checks if the result is empty
      if (result.isBeforeFirst()) {
        while (result.next()) {
          Recipe recipe = getRecipe(result.getInt("recipeid"));
          plannedRecipes.add(recipe);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("An error occurred while getting the planned recipes: " + e.getMessage());
    }
    return plannedRecipes;
  }
  
  /**
   * Closes the connection and updates the database with the user's inventory and shopping cart.
   */
  
  public void closeConnection() {
    
    try {
      int id = getUserId(session.getUser().getEmail());
      
      updateInventory(session.getInventory(), id);
      updateCookbook(session.getCookbook(), id);
      updateShoppingCart(session.getShoppingCart(), id);
      updatePlannedRecipes(session.getPlannedRecipes(), id);
      
    } catch (Exception e) {
      System.out.println("An error occurred while updating the database: " + e.getMessage());
    }
    
    try {
      connection.close();
    } catch (Exception e) {
      System.out.println("An error occurred while closing the connection: " + e.getMessage());
    }
    System.out.println("Connection closed");
  }
  
  public void updateDatabaseIngredientList(String tableName, IngredientsList ingredientsList,
                                           int userId) {
    try {
      String query = "DELETE FROM " + tableName + " WHERE userid = " + userId;
      executeUpdate(query);
      HashMap<Ingredient, Integer> ingredients = ingredientsList.getIngredients();
      for (Ingredient ingredient : ingredients.keySet()) {
        query = "INSERT INTO " + tableName + " (userid, ingredientid, quantity) VALUES (" + userId +
            ", " + ingredient.getId() + ", " + ingredients.get(ingredient) + ")";
        executeUpdate(query);
      }
    } catch (Exception e) {
      System.out.println("An error occurred while updating the database: " + e.getMessage());
    }
  }
  
  public void updateShoppingCart(ShoppingCart shoppingCart, int userId) {
    updateDatabaseIngredientList(
        "shoppingCart",
        shoppingCart,
        userId
    );
  }
  
  public void updateInventory(Inventory inventory, int userId) {
    updateDatabaseIngredientList(
        "inventory",
        inventory,
        userId
    );
  }
  
  public void updatePlannedRecipes(ArrayList<Recipe> plannedRecipes, int userId) {
    updateRecipeList(
        "plannedRecipes",
        plannedRecipes,
        userId
    );
  }
  
  public void updateCookbook(Cookbook cookbook, int userId) {
    updateRecipeList(
        "cookbook",
        cookbook.getRecipes(),
        userId
    );
  }
  
  public void updateRecipeList(String tableName, ArrayList<Recipe> recipes, int userId) {
    try {
      String query = "DELETE FROM " + tableName + " WHERE userid = " + userId;
      executeUpdate(query);
      recipes.forEach(recipe -> executeUpdate(
          "INSERT INTO " + tableName + " (userid, recipeid) VALUES (" + userId + ", " +
              recipe.getId() + ")"));
    } catch (Exception e) {
      System.out.println("An error occurred while updating the database: " + e.getMessage());
    }
  }
  
  private void executeUpdate(String query) {
    try {
      Statement statement = connection.createStatement();
      statement.executeUpdate(query);
    } catch (Exception e) {
      System.out.println("An error occurred while executing the query: " + e.getMessage());
    }
  }
}