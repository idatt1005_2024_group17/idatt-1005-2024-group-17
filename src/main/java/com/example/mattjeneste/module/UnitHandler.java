package com.example.mattjeneste.module;

import java.util.HashMap;

public class UnitHandler {
  public enum Units {
    GRAM,
    KILOGRAM,
    LITER,
    DECILITER,
    MILLILITER,
    PIECE,
    TEASPOON,
    TABLESPOON,
    PINCH
  }
  
  public static HashMap<Units, String> unitAbbreviations = new HashMap<Units, String>() {{
    put(Units.GRAM, "g");
    put(Units.KILOGRAM, "kg");
    put(Units.LITER, "L");
    put(Units.DECILITER, "dL");
    put(Units.MILLILITER, "mL");
    put(Units.PIECE, "pcs");
    put(Units.TEASPOON, "tsp");
    put(Units.TABLESPOON, "tbsp");
    put(Units.PINCH, "pinch");
  }};
  
  /**
   * Returns the abbreviation of a unit from a HashMap
   *
   * @param unit The unit to be abbreviated
   * @return The abbreviation of the unit
   */
  
  public static String abbreviate(Units unit) {
    return unitAbbreviations.get(unit);
  }
  
}

