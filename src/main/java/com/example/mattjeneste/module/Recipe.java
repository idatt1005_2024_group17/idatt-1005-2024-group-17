package com.example.mattjeneste.module;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The Recipe class represents a recipe with a title, description, and instructions.
 *
 * @author Sara Taghypour
 * @version 1.0
 */
public class Recipe extends IngredientsList {
  
  final private int id;
  private final String title;
  private final String description;
  private final String instructions;
  private final String imagePath;
  
  /**
   * Construct a new Recipe object with the specified title, description, and instructions.
   *
   * @param title        the title of the recipe.
   * @param description  the description of the recipe.
   * @param instructions the instructions to prepare the recipe.
   */
  public Recipe(int id, String title, String description, String instructions)
      throws IllegalArgumentException {
    this(
        id,
        title,
        description,
        instructions,
        "default.jpg"
    );
  }
  
  /**
   * Construct a new Recipe object with the specified title, description, instructions, and image path.
   *
   * @param title        the title of the recipe.
   * @param description  the description of the recipe.
   * @param instructions the instructions to prepare the recipe.
   * @param imagePath    the path to the image of the recipe.
   */
  public Recipe(int id, String title, String description, String instructions, String imagePath)
      throws IllegalArgumentException {
    super();
    
    if (id < 0) {
      throw new IllegalArgumentException("ID must be a positive integer.");
    }
    if (title == null || title.isEmpty()) {
      throw new IllegalArgumentException("Title cannot be null or empty.");
    }
    
    this.id = id;
    this.title = title;
    this.description = description;
    this.instructions = instructions;
    
    if (imagePath == null || imagePath.isEmpty()) {
      imagePath = "default.jpg";
    }
    this.imagePath = "/images/" + imagePath;
  }
  
  /**
   * Construct a new Recipe from a result set from the database.
   *
   * @param resultSet the result set from the database.
   */
  
  public Recipe(ResultSet resultSet) throws SQLException {
    this(
        resultSet.getInt("id"),
        resultSet.getString("recipe.name"),
        resultSet.getString("description"),
        resultSet.getString("instructions"),
        resultSet.getString("imagePath")
    );
  }
  
  public boolean equals(Recipe recipe) {
    return this.id == recipe.id;
  }
  
  public int getId() {
    return id;
  }
  
  /**
   * Retrieve the title of the recipe.
   *
   * @return the title of the recipe.
   */
  public String getTitle() {
    return title;
  }
  
  /**
   * Retrieve the description of the recipe.
   *
   * @return the description of the recipe.
   */
  public String getDescription() {
    return description;
  }
  
  /**
   * Retrieve the instructions in order to prepare the recipe.
   *
   * @return the instructions to prepare the recipe.
   */
  public String getInstructions() {
    return instructions;
  }
  
  /**
   * Retrieve the path to the image of the recipe.
   *
   * @return the path to the image of the recipe.
   */
  public String getImagePath() {
    return imagePath;
  }
}
