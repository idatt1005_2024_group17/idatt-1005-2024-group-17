package com.example.mattjeneste.module;

import com.example.mattjeneste.module.UnitHandler.Units;

/**
 * A class representing an ingredient in a recipe.
 */
public class Ingredient {
  private final String name;
  private final Units unit;
  private final int id;
  
  /**
   * Creates an ingredient with the given name, unit, and database id.
   *
   * @param name the name of the ingredient.
   * @param unit the unit of the ingredient.
   * @param id   the id of the ingredient.
   * @throws IllegalArgumentException if the name is null or empty, the unit is null, or the id is negative.
   */
  public Ingredient(String name, Units unit, int id) throws IllegalArgumentException {
    if (id < 0) {
      throw new IllegalArgumentException("ID must be a positive integer.");
    }
    if (name == null || name.isEmpty()) {
      throw new IllegalArgumentException("Name cannot be null or empty.");
    }
    if (unit == null) {
      throw new IllegalArgumentException("Unit cannot be null.");
    }
    
    this.name = name;
    this.unit = unit;
    this.id = id;
  }
  
  /**
   * Compares this ingredient to another ingredient.
   *
   * @param ingredient the ingredient to compare to.
   * @return true if the ingredients have the same id, false otherwise.
   */
  
  public boolean equals(Ingredient ingredient) {
    return this.id == ingredient.getId();
  }
  
  public String getName() {
    return name;
  }
  
  public Units getUnit() {
    return unit;
  }
  
  public int getId() {
    return id;
  }
}
