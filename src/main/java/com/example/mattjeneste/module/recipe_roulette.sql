-- noinspection SqlNoDataSourceInspectionForFile

-- user(id, username, email, password)
-- ingredient(id, name, unit)
-- recipe(id, name, ingredientsListid, description, instructions)
-- recipeIngredients(id, ingredientid, recipeid, quantity)
-- userInventory(id, ingredientid, userid, quantity)
-- userShoppingCart(id, ingredientid, userid, quantity)


DROP TABLE IF EXISTS ShoppingCart;
DROP TABLE IF EXISTS Inventory;
DROP TABLE IF EXISTS recipeIngredients;
DROP TABLE IF EXISTS recipe;
DROP TABLE IF EXISTS ingredient;
DROP TABLE IF EXISTS user;

CREATE TABLE user
(
    id       INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    email    VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE ingredient
(
    id   INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    unit VARCHAR(255) NOT NULL
);

CREATE TABLE recipe
(
    id                INT PRIMARY KEY AUTO_INCREMENT,
    name              VARCHAR(255) NOT NULL,
    ingredientsListid INT          NOT NULL,
    description       TEXT         NOT NULL,
    instructions      TEXT         NOT NULL,
    imagePath         TEXT
);

CREATE TABLE recipeIngredients
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    ingredientid INT NOT NULL,
    recipeid     INT NOT NULL,
    quantity     INT NOT NULL
);

CREATE TABLE inventory
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    ingredientid INT NOT NULL,
    userid       INT NOT NULL,
    quantity     INT NOT NULL
);

CREATE TABLE shoppingCart
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    ingredientid INT NOT NULL,
    userid       INT NOT NULL,
    quantity     INT NOT NULL
);

CREATE TABLE cookbook
(
    id       INT PRIMARY KEY AUTO_INCREMENT,
    recipeid INT NOT NULL,
    userid   INT NOT NULL
);

CREATE TABLE plannedRecipes
(
    id       INT PRIMARY KEY AUTO_INCREMENT,
    recipeid INT NOT NULL,
    userid   INT NOT NULL
);

-- Inserting data into the tables
INSERT INTO user (username, email, password)
VALUES ("admin", "admin@admin.com", "admin"),
       ("GardSvard", "gardalhaug@gmail.com", "password"),
       ("Matt", "matt@gmail.com", "password"),
       ("test", "test@test.com", "password") -- Do NOT delete or change the placement of this user*
;
-- The user in id spot 4 is the user that is used for testing the application.
-- Do NOT delete or change the placement of this user*


-- public enum Units {
--     GRAM,
--     KILOGRAM,
--     LITER,
--     DECILITER,
--     MILLILITER,
--     PIECE,
--     TEASPOON,
--     TABLESPOON,
--     PINCH
-- }
-- Insert ingredients using the Units enum
INSERT INTO ingredient (name, unit)
VALUES ("Milk", "DECILITER"),
       ("Egg", "PIECE"),
       ("Flour", "GRAM"),
       ("Sugar", "GRAM"),
       ("Salt", "GRAM"),
       ("Butter", "GRAM"),
       ("Yeast", "GRAM"),
       ("Vanilla", "TEASPOON"),
       ("Cinnamon", "TEASPOON"),
       ("Nutmeg", "TEASPOON"),
       ("Baking Powder", "TEASPOON"),
       ("Baking Soda", "TEASPOON"),
       ("Chocolate Chips", "GRAM"),
       ("Olive Oil", "DECILITER"),
       ("Pasta", "GRAM"),
       ("Hotdog", "PIECE"),
       ("Hotdog bun", "PIECE"),
       ("Chicken", "GRAM"),
       ("Tomato Sauce", "DECILITER"),
       ("Tomato Paste", "DECILITER"),
       ("Tomato", "PIECE"),
       ("Onion", "PIECE"),
       ("Garlic", "PIECE"),
       ("Bell Pepper", "PIECE"),
       ("Mushroom", "PIECE"),
       ("Cheese", "GRAM"),
       ("Bread", "PIECE"),
       ("Rice", "GRAM"),
       ("Soy Sauce", "DECILITER"),
       ("Beef", "GRAM"),
       ("Pork", "GRAM"),
       ("Lamb", "GRAM"),
       ("Fish", "GRAM"),
       ("Shrimp", "GRAM"),
       ("Lemon", "PIECE"),
       ("Lime", "PIECE"),
       ("Orange", "PIECE"),
       ("Apple", "PIECE"),
       ("Banana", "PIECE"),
       ("Strawberry", "PIECE"),
       ("Blueberry", "PIECE"),
       ("Raspberry", "PIECE"),
       ("Blackberry", "PIECE"),
       ("Pineapple", "PIECE"),
       ("Mango", "PIECE"),
       ("Peach", "PIECE"),
       ("Plum", "PIECE"),
       ("Cherry", "PIECE"),
       ("Grape", "PIECE"),
       ("Watermelon", "PIECE"),
       ("Cantaloupe", "PIECE"),
       ("Honeydew", "PIECE"),
       ("Coconut", "PIECE"),
       ("Avocado", "PIECE"),
       ("Pumpkin", "PIECE"),
       ("Squash", "PIECE"),
       ("Zucchini", "PIECE"),
       ("Carrot", "PIECE"),
       ("Celery", "PIECE"),
       ("Cucumber", "PIECE"),
       ("Lettuce", "PIECE"),
       ("Spinach", "PIECE"),
       ("Kale", "PIECE"),
       ("Broccoli", "PIECE"),
       ("Cauliflower", "PIECE"),
       ("Brussels Sprouts", "PIECE"),
       ("Asparagus", "PIECE"),
       ("Green Beans", "PIECE"),
       ("Peas", "PIECE"),
       ("Corn", "PIECE"),
       ("Potato", "PIECE"),
       ("Sweet Potato", "PIECE"),
       ("Yam", "PIECE"),
       ("Beet", "PIECE"),
       ("Radish", "PIECE"),
       ("Turnip", "PIECE"),
       ("Parsnip", "PIECE"),
       ("Eggplant", "PIECE"),
       ("Bell Pepper", "PIECE"),
       ("Jalapeno", "PIECE"),
       ("Serrano", "PIECE"),
       ("Habanero", "PIECE"),
       ("Poblano", "PIECE"),
       ("Anaheim", "PIECE"),
       ("Cayenne", "PIECE"),
       ("Paprika", "PIECE"),
       ("Chili Powder", "PIECE"),
       ("Cumin", "PIECE"),
       ("Coriander", "PIECE"),
       ("Cilantro", "PIECE"),
       ("Parsley", "PIECE"),
       ("Basil", "PIECE"),
       ("Oregano", "PIECE"),
       ("Thyme", "PIECE"),
       ("Rosemary", "PIECE"),
       ("Sage", "PIECE"),
       ("Mint", "PIECE"),
       ("Dill", "PIECE"),
       ("Bay Leaf", "PIECE"),
       ("Cinnamon", "PIECE"),
       ("Nutmeg", "PIECE"),
       ("Ginger", "PIECE"),
       ("Cloves", "PIECE"),
       ("Allspice", "PIECE"),
       ("Cardamom", "PIECE"),
       ("Vanilla", "PIECE"),
       ("Almond", "PIECE"),
       ("Peanut", "PIECE"),
       ("Cashew", "PIECE"),
       ("Walnut", "PIECE"),
       ("Pecan", "PIECE"),
       ("Pistachio", "PIECE"),
       ("Macadamia", "PIECE"),
       ("Hazelnut", "PIECE"),
       ("Coconut", "PIECE")
;

-- Insert recipes
INSERT INTO recipe (name, ingredientsListid, description, instructions, imagePath)
VALUES ("Pancakes", 1, "Good ol' pancakes", "Mix all the ingredients together and fry them in a pan", "pancakes.png"),
       ("Chocolate Chip Cookies", 2, "Like grandma used to make",
        "Mix all the ingredients together and bake them in the oven", "cookies.png"),
       ("Spaghetti", 3, "Italian style", "Boil the pasta and fry the hotdog", "spaghetti.png"),
       ("Chicken", 4, "Fried and crunchy", "Fry the chicken in a pan", "friedChicken.jpeg"),
       ("Hotdog", 5, "Boring, we know", "Fry the hotdog in a pan", "hotdogs.png"),
       ("Pasta", 6, "Like spaghetti, but not", "Boil the pasta and fry the chicken", "pasta.png"),
       ("Pizza", 7, "The classic", "Bake the pizza in the oven", "pizza.png"),
       ("Burger", 8, "Big and juicy", "Fry the burger in a pan", "burger.png"),
       ("Taco", 9, "Mexican crunch", "Fry the taco in a pan", "taco.png"),
       ("Burrito", 10, "Filling and simple", "Fry the burrito in a pan", "burrito.png"),
       ("Quesadilla", 11, "Cheesy goodness", "Fry the quesadilla in a pan", "quesadilla.png"),
       ("Enchilada", 12, "Beans.", "Fry the enchilada in a pan", "enchilada.png")
;

-- Insert recipe ingredients, just 4-5 ingredients for each recipe with random ingredients up to id 100
INSERT INTO recipeIngredients (ingredientid, recipeid, quantity)
VALUES (1, 1, 2),
       (2, 1, 1),
       (3, 1, 2),
       (4, 1, 1),
       (13, 2, 2),
       (6, 2, 1),
       (4, 2, 1),
       (5, 2, 1),
       (14, 2, 1),
       (15, 3, 2),
       (16, 3, 1),
       (17, 3, 1),
       (73, 4, 1),
       (59, 4, 5),
       (98, 4, 3),
       (36, 4, 3),
       (38, 4, 4),
       (60, 4, 3),
       (62, 5, 1),
       (74, 5, 3),
       (89, 5, 4),
       (31, 5, 3),
       (54, 5, 2),
       (93, 6, 6),
       (17, 6, 4),
       (68, 6, 5),
       (20, 6, 3),
       (6, 6, 1),
       (99, 7, 3),
       (46, 7, 5),
       (19, 7, 5),
       (57, 7, 2),
       (72, 7, 4),
       (18, 8, 1),
       (33, 8, 2),
       (46, 8, 6),
       (7, 8, 3),
       (54, 8, 5),
       (21, 9, 2),
       (25, 9, 2),
       (15, 9, 2),
       (26, 9, 1),
       (66, 9, 4),
       (49, 10, 2),
       (88, 10, 4),
       (74, 10, 5),
       (54, 10, 6),
       (41, 10, 3),
       (60, 11, 3),
       (12, 11, 3),
       (78, 11, 2),
       (63, 11, 4),
       (38, 11, 5),
       (40, 12, 1),
       (19, 12, 5),
       (11, 12, 6),
       (67, 12, 5),
       (64, 12, 2)
;

-- Insert user inventory
INSERT INTO inventory (ingredientid, userid, quantity)
VALUES (1, 2, 1),
       (2, 2, 2),
       (3, 2, 2),
       (4, 2, 1),
       (5, 2, 1),
       (6, 2, 1),
       (7, 1, 2),
       (8, 1, 1),
       (9, 1, 1),
       (10, 1, 1),
       (11, 1, 1),
       (12, 3, 1),
       (13, 3, 1),
       (14, 3, 1),
       (15, 3, 1),
       (16, 3, 1),
       (17, 3, 1),
       (1, 4, 1),
       (2, 4, 2),
       (1, 4, 1)
;

-- Insert user shopping cart
INSERT INTO shoppingCart (ingredientid, userid, quantity)
VALUES (1, 4, 1)
;

-- Insert user cookbook
INSERT INTO cookbook (recipeid, userid)
VALUES (1, 4)
;

-- Insert user planned recipes
INSERT INTO plannedRecipes (recipeid, userid)
VALUES (1, 4)
;
