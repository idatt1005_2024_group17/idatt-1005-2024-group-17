package com.example.mattjeneste.module;

/**
 * A class representing a 2D vector.
 * In this application, it is only used to represent the size of the window.
 */
public class Vector2D {
  
  private final double x;
  private final double y;
  
  public Vector2D(double x, double y) {
    this.x = x;
    this.y = y;
  }
  
  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }
  
  public boolean equals(Vector2D vector) {
    return x == vector.getX() && y == vector.getY();
  }
  
  @Override
  public String toString() {
    return "(" + x + ", " + y + ")";
  }
}
